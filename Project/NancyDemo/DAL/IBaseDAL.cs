﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IBaseDAL<T>where T :class
    {
        //添加学生
        void AddModel(T model);
        //根据ID查找
        T SearchModelByID(int ID, string IdClos);
        //根据任意属性查找
        List<T> SearchModel(string SearchValue, string Clos);
        //根据ID删除
        bool DeleteByID(int ID, string IdClos);
        //根据ID查到所有数据
        List<T> GetALLModel();
        //根据ID跟新数据其它字段
        bool UpDateByID(int ID,string IDClos,string UpdateValue,string UpdateClos);

    }
}
