﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class BaseDAL<T> : IBaseDAL<T> where T : class
    {
        #region 单例定义
        private static object lockObject = new object();
        protected BaseDAL()
        {
        }

        private static volatile BaseDAL<T> _instance;
        public static BaseDAL<T> Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new BaseDAL<T>());
                    }
                }
                return _instance;
            }
        }
        #endregion 单例定义

        public static object synLock = new object();

        private static List<T> lstModel = new List<T>();
        /// <summary>
        /// 添加学生实体
        /// </summary>
        public void AddModel(T model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("BaseDAL/AddStudent model is null");
            }

            lstModel.Add(model);
        }
        /// <summary>
        /// 根据ID获取到学生实体
        /// </summary>
        /// <param name="ID">ID号</param>
        /// <param name="IdClos">ID字段名称</param>
        /// <returns></returns>
        public T SearchModelByID(int ID,string IdClos)
        {
            if (ID <= 0)
            {
                throw new ArgumentNullException("BaseDAL/SearchStudent ID is 0");
            }

            T model = this.Find(ID,IdClos);
            return model;
        }
        /// <summary>
        /// 根据ID查到model 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="IdClos"></param>
        /// <returns></returns>
        private T Find(int ID, string IdClos)
        {
            Type type = typeof(T);
            if (type == null)
            {
                throw new ArgumentNullException("BaseDAL/Find type is null");
            }
            T model = lstModel.Find(s =>
            {
                PropertyInfo propertyInfo = type.GetProperty(IdClos); //获取指定名称的属性
                if (propertyInfo == null)
                {
                    throw new ArgumentNullException("BaseDAL/Find propertyInfo is null");
                }

                object objValue = propertyInfo.GetValue(s);
                if (objValue.ToString() == ID.ToString())
                {
                    return true;
                }

                return false;
            });
            return model;
        }

        /// <summary>
        /// 根据任意字段查询
        /// </summary>
        /// <param name="SearchValue">字段值</param>
        /// <param name="Clos">字段名称</param>
        /// <returns></returns>

        public List<T> SearchModel(string SearchValue, string Clos)
        {
            if (string.IsNullOrEmpty(SearchValue) || string.IsNullOrEmpty(Clos))
            {
                throw new ArgumentNullException("BaseDAL/SearchStudent SearchValue or Clos is null");
            }

            Type type = typeof(T);
            if (type == null)
            {
                throw new ArgumentNullException("BaseDAL/SearchStudent type is null");
            }

            List<T> lstSearchModel = lstModel.Where(s =>
                {
                    PropertyInfo propertyInfo = type.GetProperty(Clos); //获取指定名称的属性
                    if (propertyInfo == null)
                    {
                        throw new ArgumentNullException("BaseDAL/SearchStudentByID propertyInfo is null");
                    }

                    object objValue = propertyInfo.GetValue(s);
                    if (objValue.ToString() == SearchValue)
                    { 
                        return true;
                    }

                    return false;
                }).ToList();
  
            return lstSearchModel;
        }
        /// <summary>
        /// 根据ID删除Model
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="IdClos"></param>
        /// <returns></returns>
        public bool DeleteByID(int ID, string IdClos)
        {
           T model = Find(ID, IdClos);
           if (model == null)
           {
               return false;
           }
           lock (synLock)
           {
              lstModel.Remove(model);
           }
           return true;
        }

        /// <summary>
        /// 查询所有数据
        /// </summary>
        /// <returns></returns>
        public List<T> GetALLModel()
        {
            return lstModel;
        }
        /// <summary>
        /// 内部修改函数
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UpdateValue"></param>
        /// <param name="UpdateClos"></param>
        private void UpdateByClos(T model,string UpdateValue, string UpdateClos)
        {
            Type type = typeof(T);
            PropertyInfo propertyInfo = type.GetProperty(UpdateClos); //获取指定名称的属性
            if (propertyInfo == null)
            {
                throw new ArgumentNullException("BaseDAL/UpdateByClos model is null");
            }

            lock (synLock)
            {
                propertyInfo.SetValue(model, UpdateValue);
            }
        }
        /// <summary>
        /// 根据ID修改其它字段
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="IDClos"></param>
        /// <param name="UpdateValue"></param>
        /// <param name="UpdateClos"></param>
        /// <returns></returns>
        public bool UpDateByID(int ID, string IDClos, string UpdateValue, string UpdateClos)
        {
            T model = Find(ID, IDClos);
            if (model == null)
            {
                return false;
            }

           this.UpdateByClos(model, UpdateValue, UpdateClos);
           return true;
        }
    }
}
