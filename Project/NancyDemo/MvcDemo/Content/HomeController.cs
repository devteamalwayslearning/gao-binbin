﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace MvcDemo.Content
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        [ValidateAntiForgeryToken] 
        public ActionResult Index()
        {
      
            return View();
        }

    }
}
