﻿
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// 学生服务
    /// </summary>
    public interface IStudentService 
    {
        //添加学生
        void InsertStudent(Student addStudent);
        //根据ID找到修改的学生
        Student FindModifyStudent(int ID);
        //根据ID删除学生
        bool DeleteStudent(int ID);
        //根据ID修改学生的姓名
        List<Student> SearchAndModify(Student modifyStudent);
        //根据姓名查到学生
        List<Student> SearchByName(string Name);
        //查到所有的学生
        List<Student> GetAllStudent();
    }
}
