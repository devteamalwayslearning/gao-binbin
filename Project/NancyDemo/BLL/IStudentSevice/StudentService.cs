﻿using DAL;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class StudentService : IStudentService
    {
        private IBaseDAL<Student> iBaseDAL = BaseDAL<Student>.Instance;
        public void InsertStudent(Student addStudent)
        {
            if (addStudent== null)
            {
                throw new ArgumentNullException("StudentService/InsertStudent addStudent is null");
            }

            iBaseDAL.AddModel(addStudent);
        }

        public Student FindModifyStudent(int ID)
        {
            if(ID<=0)
            {
                throw new ArgumentNullException("StudentService/FindModifyStudent ID is 0");
            }

            var model= iBaseDAL.SearchModelByID(ID, "ID");
            return model;
        }

        public bool DeleteStudent(int ID)
        {
            if (ID <= 0)
            {
                throw new ArgumentNullException("StudentService/DeleteStudent ID is 0");
            }

            return iBaseDAL.DeleteByID(ID,"ID");
        }

        public List<Student> SearchAndModify(Student modifyStudent)
        {
             iBaseDAL.UpDateByID(modifyStudent.ID, "ID", modifyStudent.Name,"Name");
             return iBaseDAL.GetALLModel();
        }

        public List<Student> SearchByName(string Name)
        {
            if (string.IsNullOrEmpty(Name))
            {
                throw new ArgumentNullException("StudentService/DeleteStudent Name is null");
            }

            return iBaseDAL.SearchModel(Name, "Name");
        }


        public List<Student> GetAllStudent()
        {
            return iBaseDAL.GetALLModel();
        }
    }
}
