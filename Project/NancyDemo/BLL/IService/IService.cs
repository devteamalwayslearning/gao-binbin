﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public interface IService<T>where T:class
    {
        //添加模型
        void InsertModel(T model);
        //根据ID找到修改的模型
        T FindModelByID(int ID,string IDClos);
        //根据ID删除模型
        bool DeleteModel(int ID, string IDClos);
        //根据模型ID修改要修改的字段
        bool SearchAndModify(int ID,string IDClos,string UpDateValue,string UpdateClos);
        //根据字段查询查模型
        List<T> SearchByClos(string searchValue,string SearchClos);
        //查到所有的学生
        List<T> GetAllModel();
    }
}
