﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.IService
{
    public class Service<T> : IService<T> where T : class
    {


        private ICovert _iCovert;

        public Service(ICovert iCovert)
        {
            _iCovert = iCovert;
        }



        private IBaseDAL<T> iBaseDAL = BaseDAL<T>.Instance;
        /// <summary>
        /// 插入数据库
        /// </summary>
        /// <param name="addModel"></param>
        public void InsertModel(T addModel)
        {
            if (addModel == null)
            {
                throw new ArgumentNullException("Service/IBaseDAL addModel is null");
            }
            _iCovert.Say();
            iBaseDAL.AddModel(addModel);
        }
        /// <summary>
        /// 根据ID查找
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="IDClos"></param>
        /// <returns></returns>
        public T FindModelByID(int ID, string IDClos)
        {
            if (ID <= 0 || string.IsNullOrEmpty(IDClos))
            {
                throw new ArgumentNullException("Service/FindModifyModel ID is 0 or IDClos is null");
            }

            return iBaseDAL.SearchModelByID(ID,IDClos);
        }
        /// <summary>
        /// 根据ID删除
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="IDClos"></param>
        /// <returns></returns>
        public bool DeleteModel(int ID, string IDClos)
        {
            if (ID <= 0 || string.IsNullOrEmpty(IDClos))
            {
                throw new ArgumentNullException("Service/DeleteModel ID is 0 or IDClos is null");
            }

            return iBaseDAL.DeleteByID(ID,IDClos);
        }
        /// <summary>
        /// 根据ID查到某个对象，更新它的字段
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="IDClos"></param>
        /// <param name="UpDateValue"></param>
        /// <param name="UpdateClos"></param>
        /// <returns></returns>
        public bool SearchAndModify(int ID, string IDClos, string UpDateValue, string UpdateClos)
        {
            if (ID <= 0 || string.IsNullOrEmpty(IDClos) || string.IsNullOrEmpty(UpDateValue) || string.IsNullOrEmpty(UpdateClos))
            {
                throw new ArgumentNullException("Service/DeleteModel ID is 0,or IDClos or UpDateValueor UpdateClos is null");
            }

            return iBaseDAL.UpDateByID(ID,IDClos,UpDateValue,UpdateClos);
        }
        /// <summary>
        /// 根据某个字段查找Model
        /// </summary>
        /// <param name="searchValue"></param>
        /// <param name="SearchClos"></param>
        /// <returns></returns>
        public List<T> SearchByClos(string searchValue, string SearchClos)
        {
            if (string.IsNullOrEmpty(searchValue) || string.IsNullOrEmpty(SearchClos))
            {
                throw new ArgumentNullException("Service/SearchByClos searchValue or searchValue is null");
            }

            return iBaseDAL.SearchModel(searchValue, SearchClos);
        }
        /// <summary>
        /// 查找所有模型
        /// </summary>
        /// <returns></returns>
        public List<T> GetAllModel()
        {
            return iBaseDAL.GetALLModel(); ;
        }
    }
}
