﻿using Nancy;
using Nancy.Authentication.Forms;
using Nancy.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.Model
{
    public class Posts
    {
        public string[] Tags;
        public int[] Ints;
    }
    ////继承安全接口
    //public class User : IUserIdentity
    //{
    //    public User()
    //    {
    //    }
    //    public User(string userName)
    //    {
    //        UserName = userName;
    //    }
    //    /// <summary>
    //    /// 实现接口的成员
    //    /// </summary>
    //    public string UserName
    //    {
    //        get;
    //        set;
    //    }

    //    public string Password
    //    {
    //        get;
    //        set;
    //    }

    //    public string Role
    //    {
    //        get;
    //        set;
    //    }
    //    /// <summary>
    //    /// 这个是Nancy授权中必须的
    //    /// </summary>
    //    public Guid GUID
    //    {
    //        get;
    //        set;
    //    }
    //    /// <summary>
    //    /// 实现接口的成员
    //    /// </summary>
    //    public IEnumerable<string> Claims
    //    {
    //        get;
    //        set;
    //    }
    //}
    ////验证
    //   public  class UserMapper : IUserMapper
    //   {
    //    //这个用户集合可以来自数据库
    //    static List<User> _users;
    //    static UserMapper()
    //    {
    //        //初始化验用户数据
    //        _users = new List<User>();
    //        _users.Add(new User() { UserName = "aaa", Password = "111111", GUID = new Guid("33e67c06-ed6e-4bd7-a5b0-60ee0fea890c"), Role = "admin" });
    //        _users.Add(new User() { UserName = "bbb", Password = "222222", GUID = new Guid("fba4be7d-25af-416e-a641-a33174f435d1"), Role = "user" });
    //    }
    //    /// <summary>
    //    /// 实现接口的成员，获取用户的角色，以便验证授权
    //    /// </summary>
    //    /// <param name="identifier"></param>
    //    /// <param name="context"></param>
    //    /// <returns></returns>
    //    public IUserIdentity GetUserFromIdentifier(Guid identifier, NancyContext context)
    //    {
  
    //        var user = _users.FirstOrDefault(u => u.GUID == identifier);
    //        return user == null
    //                     ? null
    //                     : new User(user.UserName) { Claims = new string[] { user.Role } };
    //    }
    //    /// <summary>
    //    /// 验证用户，验证成功返回用户的GUID，这是规定
    //    /// </summary>
    //    /// <param name="userName"></param>
    //    /// <param name="password"></param>
    //    /// <returns></returns>
    //    public static Guid? ValidateUser(string userName, string password)
    //    {
    //        var user = _users.FirstOrDefault(u => u.UserName == userName && u.Password == password);
  
    //        if (user == null)
    //        {
    //            return null;
    //        }
    //        return user.GUID;
    //    }
   // }
}
