﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.Model
{
    public class JsonResult
    {
        public int State { get; set; }
        public string Message { get; set; }
        public object Result { get; set; }
    }
}
