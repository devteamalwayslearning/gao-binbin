﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.Config
{
    //文件路径
    public class CustomRootPathProvider : IRootPathProvider
    {
        public string GetRootPath()
        {
            //取出路径然后找到绝对路径
            return AppDomain.CurrentDomain.BaseDirectory;
        }
    }
}
