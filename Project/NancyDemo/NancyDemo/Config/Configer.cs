﻿using Library;
using Library.IService;
using Entity;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Conventions;
//IOC控制反转
using Nancy.TinyIoc;
using NancyDemo.AjaxStudentBLL;
using NancyDemo.ConvertModel;
using NancyDemo.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy.Authentication.Forms;
using NancyDemoForFormsauthentication;
using Nancy.Security;
using log4net.Config;
namespace NancyDemo.Config
{
    public class Configer : DefaultNancyBootstrapper
    {
        protected override void ConfigureRequestContainer(TinyIoCContainer container, NancyContext context)
        {
             base.ConfigureRequestContainer(container, context);
             container.Register<IUserMapper, UserMapper>();
        }
      /// <summary>
        /// Show err message
        /// </summary>
        /// <param name="container"></param>
        /// <param name="pipelines"></param>
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);
            StaticConfiguration.EnableRequestTracing = true;
            StaticConfiguration.DisableErrorTraces = false;
            //Create a NancyDefaultBootstrapper class or use your existing. 
            //Add the following line to your ApplicationStartup() method:
            //启用AntiToken
            Csrf.Enable(pipelines);


            base.ConfigureApplicationContainer(container);
            //依赖注入
            //container.Register<ISearchModifyStudent, AjaxSearchModifyStudent>().AsSingleton();
            //container.Register<IAddStudent, AjaxAddStudent>().AsSingleton();
            //container.Register<IFindModifyStudent, AjaxModifyStudent>().AsSingleton();
            //container.Register<IDeleteStudent, AjaxDeleteStudent>().AsSingleton();
            //container.Register<ISearchStudent, AjaxSearchStudent>().AsSingleton();

            container.Register<IStudentService, StudentService>().AsSingleton();
            //container.Register<ICovert, Covert>().AsSingleton();
            //利用Type实现IOC
            container.Register(typeof(ICovert), typeof(Covert)).AsSingleton();
            container.Register<IConvertModel<Student, viewStudent>, ConvertToViewStudent>().AsSingleton();
            container.Register<IService<ListItem>, Service<ListItem>>().AsSingleton();
            container.Register<IConvertModel<ListItem, viewListItem>, ConvertToViewTaskItem>().AsSingleton();

            //验证开启和调用
            var formsAuthConfiguration = new FormsAuthenticationConfiguration()
            {
                RedirectUrl = "~/login",//无验证跳转的路由
                UserMapper = container.Resolve<IUserMapper>()//用户对应的Mapper
            };
            //开启验证
            FormsAuthentication.Enable(pipelines, formsAuthConfiguration);

            //应用程序启动时，自动加载配置log4Net 
            XmlConfigurator.Configure();  
        }

        protected override IRootPathProvider RootPathProvider
        {
            get { return new CustomRootPathProvider(); }
        }

        //protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        //{
        //    base.ConfigureApplicationContainer(container);
        //    container.Register<ISearchStudent, SearchStudentProvider>().AsSingleton();
        //}

        //静态文件配置
        protected override void ConfigureConventions(NancyConventions conventions)
        {
            base.ConfigureConventions(conventions);

            conventions.StaticContentsConventions.Add(
                StaticContentConventionBuilder.AddFile("Content", @"Content")
            );
             conventions.ViewLocationConventions.Add((viewName, model, context) => string.Concat("Views/", viewName));
        }
      
      
        /// <summary>
        /// AOP控制器前检验
        /// </summary>
        /// <param name="container"></param>
        /// <param name="pipelines"></param>
        /// <param name="context"></param>
        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            base.RequestStartup(container, pipelines, context);
            pipelines.BeforeRequest += ctx =>
            {
                
                string url = context.Request.Url;
                var a = ctx;
                object obj = a;


                return null;
            };

            pipelines.AfterRequest += ctx =>
            {
                //if (ctx.CurrentUser == null || !ctx.CurrentUser.IsAuthenticated()) return;
                var a = ctx;
                object obj = a;
                //obj = obj;
               IUserIdentity iUserIdentity = ctx.CurrentUser;
               
                return;
            };

            pipelines.OnError += (ctx, ex) =>
            {
                string strd = ex.Message.ToString();
                StreamWriter streamWriter = new StreamWriter(@"D:\Err.txt", true);
                streamWriter.Write(strd);
                streamWriter.Flush();
                streamWriter.Close();

            
                return null;
            };
            
        }
    }
}
