﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.ConvertModel
{
    /// <summary>
    /// Student模型转化成视图student
    /// </summary>
    public class ConvertToViewStudent : IConvertModel<Student, viewStudent>
    {
        /// <summary>
        /// 单个转化
        /// </summary>
        /// <param name="model">数据库模型</param>
        /// <returns>视图模型</returns>
        public viewStudent ConvertToViewModel(Student model)
        {
            viewStudent viewStudent = new viewStudent();
            viewStudent.ID = model.ID;
            viewStudent.Name = model.Name;
            return viewStudent;
        }
        /// <summary>
        /// lst转化
        /// </summary>
        /// <param name="lstModel">数据库模型List</param>
        /// <returns>视图模型List</returns>
        public List<viewStudent> CovertLstViewModel(List<Student> lstModel)
        {
            List<viewStudent> lstviewStudent = new List<viewStudent>();
            
            if (lstModel.Count ==0)
            {
                return lstviewStudent;
            }

            lstModel.ForEach(s =>
                {
                    viewStudent viewStudent = new viewStudent();
                    viewStudent.ID = s.ID;
                    viewStudent.Name = s.Name;
                    lstviewStudent.Add(viewStudent);
                });
            return lstviewStudent;
        }
    }
}
