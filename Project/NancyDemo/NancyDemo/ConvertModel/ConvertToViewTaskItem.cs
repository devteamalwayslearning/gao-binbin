﻿using Entity;
using NancyDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.ConvertModel
{
    public class ConvertToViewTaskItem : ConvertModelProvider<ListItem,viewListItem> 
    {
        //model转化成viewmodel
        protected override viewListItem ConvertToViewModelLogic(ListItem model)
        {
            viewListItem viewTaskItem = new viewListItem();
            viewTaskItem.ItemId = model.ItemId;
            viewTaskItem.Title = model.Title;
            return viewTaskItem;
        }
        /// <summary>
        /// lst转化
        /// </summary>
        /// <param name="lstModel">数据库模型List</param>
        /// <returns>视图模型List</returns>
        protected override List<viewListItem> CovertLstViewModelLogic(List<ListItem> lstModel)
        {
            List<viewListItem> lstviewTaskItem = new List<viewListItem>();

            if (lstModel.Count == 0)
            {
                return lstviewTaskItem;
            }

            lstModel.ForEach(s =>
            {
                viewListItem viewTaskItem = new viewListItem();
                viewTaskItem.ItemId = s.ItemId;
                viewTaskItem.Title = s.Title;
                lstviewTaskItem.Add(viewTaskItem);
            });
            return lstviewTaskItem;
        }
    }
}
