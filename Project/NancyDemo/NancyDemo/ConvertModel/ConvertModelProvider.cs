﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.ConvertModel
{
    public abstract class ConvertModelProvider<T, S> : IConvertModel<T, S>
        where T : class
        where S : class
    {
        public S ConvertToViewModel(T model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("ConvertStudentModelProvider/ConvertToViewModel model is null");
            }
            return this.ConvertToViewModelLogic(model);
        }

        protected abstract S ConvertToViewModelLogic(T model);

        public List<S> CovertLstViewModel(List<T> lstModel)
        {
            if (lstModel == null)
            {
                throw new ArgumentNullException("ConvertStudentModelProvider/ConvertToViewModelLogic lstModel is null");
            }

            return this.CovertLstViewModelLogic(lstModel);
        }

        protected abstract List<S> CovertLstViewModelLogic(List<T> lstModel);
    }
}
