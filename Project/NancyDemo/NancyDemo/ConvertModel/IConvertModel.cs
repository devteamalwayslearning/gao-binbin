﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.ConvertModel
{
    /// <summary>
    /// 模型到视图模型的转化
    /// </summary>
    /// <typeparam name="T">模型</typeparam>
    /// <typeparam name="S">视图模型</typeparam>
    public interface IConvertModel<T,S>
        where T:class 
        where S :class
    {
        //数据库模型转化视图模型
        S ConvertToViewModel(T model);

        List<S> CovertLstViewModel(List<T> lstModel);
    }
}
