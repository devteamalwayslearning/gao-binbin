﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Authentication.Forms;
using Nancy.ModelBinding;
namespace NancyDemo.Controller
{
   //系统使用者
    public class SystemUser
    {
         public Guid SystemUserId { get; set; }
         public string SystemUserName { get; set; }
         public string SystemUserPassword { get; set; }
     }
    public class FormTicketModule : NancyModule
    {
        public FormTicketModule()
        {
            Get["/q"] = _ =>
            {
                return View["index"];
            };
            Get["/login"] = _ =>
            {
                return View["login"];
            };
            Post["/login"] = _ =>
            {
                var loginUser = this.Bind<SystemUser>();
                SystemUser user = GetValidUser(loginUser.SystemUserName, loginUser.SystemUserPassword);
                if (user == null)
                {
                    return Response.AsText("出错了", "text/html;charset=UTF-8");
                }
                //在这里注入框架
                return this.LoginAndRedirect(user.SystemUserId, fallbackRedirectUrl: "/secure");
            };
        }

        private SystemUser GetValidUser(string name, string pwd)
        {
            SystemUser systemUser = new SystemUser();
            systemUser.SystemUserName = name;
            systemUser.SystemUserPassword = pwd;
            return systemUser;
        }




        //private readonly string sqlconnection =
        //       "Data Source=127.0.0.1;Initial Catalog=NancyDemo;User Id=sa;Password=dream_time1314;";
        //private SqlConnection OpenConnection()
        //{
        //    SqlConnection connection = new SqlConnection(sqlconnection);
        //    connection.Open();
        //    return connection;
        //}
        //private SystemUser GetValidUser(string name, string pwd)
        //{
        //    using (IDbConnection conn = OpenConnection())
        //    {
        //        const string query = "select * from SystemUser where SystemUserName=@SystemUserName and SystemUserPassword=@SystemUserPassword";
        //        return conn.Query<SystemUser>(query, new { SystemUserName = name, SystemUserPassword = pwd }).SingleOrDefault();
        //    }
        //}
    }
}
