﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy.Extensions;
using Nancy.Security;
using NancyDemo.Model;
using System.Web.Helpers;
using Nancy.Cookies;
using log4net;
using Comm;
using Log4NetApply;

namespace NancyDemo.Controller
{
    //测试log4net配置文件
    //分布视图
    public class PartialModule : NancyModule
    {
        log4net.ILog log = log4net.LogManager.GetLogger("iNotes");
        public PartialModule()
        {

            Get["/p12"] = _ =>
            {
                Helper.Hello();

                try
                {
                    throw new ArgumentNullException("is null");
                }
                catch(Exception e)
                {
                    log.Error(new LogContent("127.0.0.1", "111111", "登陆系统", e.Message + ":" + e.StackTrace));
                    log.Info(e);
                    log.Warn("22222",e);
                }
                
                var requerst = Request;
                
                return View["index"];
            };

            Post["/post"] = _form =>
            {
                //Rca 跨站攻击
                this.CreateNewCsrfToken();
                string Name = Request.Form["name"];
           
                return View["index"];
            };

            Post["/ajax"] = ajax =>
            {
                //Nancy cookies
                Response.Context.Response.Cookies.Add(new NancyCookie("wener", "cool"));
           
                new NancyCookie("wener", "cool");
                var jsonResult = new JsonResult();
                jsonResult.State = 1;
                return Response.AsJson(jsonResult);
            };
        }
    }
}
