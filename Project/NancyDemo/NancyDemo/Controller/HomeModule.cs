﻿﻿using Nancy;
using NancyDemo;
using System;
using System.Collections.Generic;
//Add a comment to this line
using System.Linq;
using System.Text;
using System.Web;
namespace NancyDemo
{
    public class HomeModule : NancyModule
    {        //存储 bitbuket
        public static  List<viewStudent> lstStudent = new List<viewStudent>();
        public HomeModule()
           
        {

            Get["/"] = get =>
            {
                return View["/Index", lstStudent];
            };


            Get["/{ID}"] = get =>
            {
                var ID = get.ID;
                var IdSearch = 0;
                if (!int.TryParse(ID, out IdSearch))
                {
                    return View["/Index", lstStudent];
                }

                var student = lstStudent.Find(s => s.ID == IdSearch);
                if (student == null)
                {
                    return View["/Index", lstStudent];
                }

                lstStudent.Remove(student);
                return View["/Index", lstStudent];
            };

            Get["/home/modify/{ID}"] = get =>
            {
                var getID = get.ID;
                var ID=0;
                if(!int.TryParse(getID, out ID))
                {
                    return View["/Index", lstStudent];
                }

                var student = lstStudent.Find(s=>s.ID==ID);
                if (student == null)
                {
                    return View["/Index", lstStudent];
                }

                return View["/Home", student];
            };

            Post["/home/add"] = add =>
            {  
                var form = Request.Form;
                var Name = form["Name"];
                if(string.IsNullOrEmpty(Name))
                {
                    return View["/Index", lstStudent];
                }

                var student = new viewStudent();
                var random = new Random(); 
                student.ID = random.Next(10000);
                student.Name = Name;
                lstStudent.Add(student);
                return View["/Index", lstStudent];
            };


            Post["/home/search"] = search =>
            {
                var form = Request.Form;
                var Name = form["Search"];
                if (string.IsNullOrEmpty(Name))
                {
                    return View["/Index", lstStudent];
                }

                var lst1Student1 = lstStudent.Where(s => s.Name == Name).ToList();
                if (lst1Student1== null||lst1Student1.Count==0)
                {
                    return View["/Index", new List<viewStudent>()];
                }

                return View["/Index", lst1Student1];
            };

            Post["/home/modify"] = modify =>
            { 
                var form = Request.Form;
                var ID = form["hidden"];
                var Name = form["Name"];
                int idModify = 0;
                if (!int.TryParse(ID, out idModify))
                {
                    return View["/Index", lstStudent];
                }

                var student = lstStudent.Find(s => s.ID == idModify);
                if (student== null)
                {
                    return View["/Index", lstStudent];
                }

                student.Name = Name;
                return View["/Index", lstStudent];
            };
        }
    }
}