﻿using Entity;
using Library;
using Nancy;
using NancyDemo.ConvertModel;
using NancyDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.Controller
{
    public class newTodoModule : NancyModule
    {
        private IService<ListItem> _iService;
        //路径设置
        private IRootPathProvider _iRootPathProvider;
        public newTodoModule
        (
            IService<ListItem> iService,
            IRootPathProvider iRootPathProvider
            //路径使用时通过绝对路径
            //var filename = Path.Combine(uploadDirectory, file.Name);
        )
        {
           
            _iService = iService;
            _iRootPathProvider = iRootPathProvider;
            //   string path = _iRootPathProvider.GetRootPath(); 
            //显示页面
            Get["/todoview"] = allList => GetListAllView(allList);
            //获取所有的数据
            Get["/todo"] = allList => GetListAll(allList);
            //修改数据
            Put["/todo"] = modifyList => ModifyList(modifyList);
            //删除数据
            Delete["/todo"] = deleteList => DeleteList(deleteList);
            //添加数据
            Post["/todo"] = addList => AddList(addList);
            //查询数据
            Get["/todo/{search}"] = searchlist => GetListSearchName(searchlist);
       }
        /// <summary>
        /// 得到查询的数据
        /// </summary>
        /// <param name="SearchName"></param>
        /// <returns></returns>
        public dynamic GetListSearchName(dynamic SearchName)
        {
       

            var Title = SearchName.search;
            var jsonResult = new JsonResult();
            if (Title == "empty")
            {
                var lstItem = _iService.GetAllModel();
                jsonResult.Message = "Parameter is null";
                jsonResult.State = 1;
                jsonResult.Result = lstItem;
                return Response.AsJson(jsonResult);
            }

            var lstSearch = _iService.SearchByClos(Title, "Title");
            jsonResult.Result = lstSearch;
            jsonResult.State = 1;
            return Response.AsJson(jsonResult);
        }


         //<summary>
         //添加数据
         //</summary>
         //<param name="addlist"></param>
         //<returns></returns>
        public dynamic AddList(dynamic addList)
        {
            var Title = Request.Form["Title"];
            var jsonResult = new JsonResult();
            if (string.IsNullOrEmpty(Title))
            {
                jsonResult.State = 0;
                jsonResult.Message = "Parameter is null";
                return Response.AsJson(jsonResult);
            }
            var random = new Random();
            var ID =  random.Next(100);
            ListItem taskItem = new ListItem() {
                ItemId =ID,
                Title = Title
            };

            _iService.InsertModel(taskItem);
            jsonResult.State = 1;
            return Response.AsJson(jsonResult);
        }

        /// <summary>
        /// 获取页面
        /// </summary>
        /// <param name="allList"></param>
        /// <returns></returns>
        public dynamic GetListAllView(dynamic allList)
        {
            //开启全局验证
            this.RequiresAuthentication();
            return View["/newTodo"];
        }
        /// <summary>
        /// 获取所有的数据
        /// </summary>
        /// <param name="allList"></param>
        /// <returns></returns>
        public dynamic GetListAll(dynamic allList)
        {
            var lstTodo = _iService.GetAllModel();
            var jsonResult = new JsonResult();
            jsonResult.Result = lstTodo;
            jsonResult.State = 1;
            return Response.AsJson(jsonResult);
        }
        /// 修改数据
        /// </summary>
        /// <param name="modifyList"></param>
        /// <returns></returns>
        public dynamic ModifyList(dynamic modifyList)
        {
            var bSuccess = false;
            var jsonResult = new JsonResult();
            var ItemId = Request.Form["ItemId"];
            var Title = Request.Form["Title"];
            if (ItemId == 0 || string.IsNullOrEmpty(Title))
            {
                jsonResult.Result = bSuccess;
                jsonResult.Message = "Parameter is null";
                jsonResult.State = 0;
                return Response.AsJson(jsonResult);
            }

            _iService.SearchAndModify(ItemId, "ItemId", Title, "Title");
            bSuccess = true;
            jsonResult.Result = bSuccess;
            jsonResult.State = 1;
            return Response.AsJson(jsonResult);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="deleteList"></param>
        /// <returns></returns>
        public dynamic DeleteList(dynamic deleteList)
        {
            var bSuccess = false;
            var jsonResult = new JsonResult();
            var ItemId = Request.Form["ItemId"];
            if (ItemId <= 0)
            {
                return Response.AsJson(bSuccess);
            }
            _iService.DeleteModel(ItemId, "ItemId");
            bSuccess = true;
            return Response.AsJson(bSuccess);
        }
    }
}