﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.Controller
{
    public static class ModuleSecurity
    {
        public static void RequiresAuthentication(this NancyModule module)
        {
            module.Before.AddItemToEndOfPipeline(RequiresAuthentication);
        }

        private static Response RequiresAuthentication(NancyContext context)
        {
            Response response = null;
            if ((context.CurrentUser == null) ||
                String.IsNullOrWhiteSpace(context.CurrentUser.UserName))
            {
                response = new Response { StatusCode = HttpStatusCode.Unauthorized };
            }

            return response;
        }

    }
}
