﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.Controller
{
    public class AreaModule : NancyModule
    {
        public AreaModule()
            : base("/admin")
        {
            Get["/"] = _ =>
            {
           
                return View["index"];
            };
        }
    }
}
