﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.AjaxStudentBLL
{
    public class AjaxSearchStudent : SearchStudentProvider
    {
        protected override List<viewStudent> AjaxSearchStudentLogic(string SearchName, List<viewStudent> lstStudent)
        {
             List<viewStudent> lstSearchStudent = lstStudent.Where(s=>s.Name == SearchName).ToList();
             return lstSearchStudent;
        }
    }
}
