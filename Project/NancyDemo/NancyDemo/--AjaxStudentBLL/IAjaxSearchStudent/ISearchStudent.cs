﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.AjaxStudentBLL
{
    public interface ISearchStudent
    {
        List<viewStudent> AjaxSearchStudent(string SearchName, List<viewStudent> lstStudent);
    }
}
