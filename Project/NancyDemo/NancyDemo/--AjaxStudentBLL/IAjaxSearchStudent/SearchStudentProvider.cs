﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.AjaxStudentBLL
{
    public abstract class SearchStudentProvider : ISearchStudent
    {
        public List<viewStudent> AjaxSearchStudent(string SearchName,List<viewStudent> lstStudent)
        {
            if (string.IsNullOrEmpty(SearchName))
            {
                throw new ArgumentNullException("AjaxSearchStudentProvider/AjaxSearchStudent  SearchName is null");
            }
            return this.AjaxSearchStudentLogic(SearchName,lstStudent);
        }

        protected abstract List<viewStudent> AjaxSearchStudentLogic(string SearchName, List<viewStudent> lstStudent);
    }
}
