﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.AjaxStudentBLL
{
    public class AjaxSearchModifyStudent:SearchModifyStudentProvider
    {
        public static object synLock = new object();
        protected override viewStudent SearchAndModifyLogical(viewStudent student, List<viewStudent> lstnewStudent)
        {
            var modifyStudent = from searchStudent in lstnewStudent where searchStudent.ID == student.ID select searchStudent;

            if (modifyStudent == null)
            {
                throw new ArgumentNullException("Not find Student");
            }

            var lstResultStudent = modifyStudent.ToList();

            lock (synLock)
            {
                lstResultStudent[0].Name = student.Name;
            }
            return lstResultStudent[0];
        }
    }
}
