﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo
{
    public abstract class SearchModifyStudentProvider : ISearchModifyStudent
    {
        public viewStudent Search(viewStudent student, List<viewStudent> lstnewStudent)
        {
            if (student == null || lstnewStudent == null)
            {
                throw new ArgumentNullException("SearchStudentProvider/Search student or lstnewStudent is null");
            }

            return this.SearchAndModifyLogical(student, lstnewStudent);
        }


        protected abstract viewStudent SearchAndModifyLogical(viewStudent student, List<viewStudent> lstnewStudent);
 
    }
}
