﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.AjaxStudentBLL
{
    public class AjaxAddStudent : AddStudentProvider
    {
        protected override void InsertIntoLsit(viewStudent addStudent, List<viewStudent> lstStudent)
        {
            Random random = new Random();
            addStudent.ID = random.Next(1000);
            lstStudent.Add(addStudent);
        }
    }
}
