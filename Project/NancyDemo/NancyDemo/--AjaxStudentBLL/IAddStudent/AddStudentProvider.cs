﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.AjaxStudentBLL
{
    public abstract class AddStudentProvider : IAddStudent
    {
        public virtual void InsertStudent(viewStudent addStudent, List<viewStudent> lstStudent)
        {
            if (string.IsNullOrEmpty(addStudent.Name) || lstStudent == null)
            {
                throw new ArgumentNullException("Name Empty or lstStudent,Count is 0");
            }

            this.InsertIntoLsit(addStudent, lstStudent);
        }

        protected abstract void InsertIntoLsit(viewStudent addStudent, List<viewStudent> lstStudent);
    }
}
