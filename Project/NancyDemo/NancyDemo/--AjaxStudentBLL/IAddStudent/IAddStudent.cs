﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.AjaxStudentBLL
{
    public interface IAddStudent
    {
        void InsertStudent(viewStudent addStudent, List<viewStudent> lstStudent);
    }
}
