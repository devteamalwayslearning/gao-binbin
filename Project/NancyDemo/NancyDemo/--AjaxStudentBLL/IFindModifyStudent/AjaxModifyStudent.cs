﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.AjaxStudentBLL
{
    public class AjaxModifyStudent : FindModifyStudentProvider
    {
        protected override viewStudent FindStudentLogic(int ID, List<viewStudent> lstStudent)
        {
            var modifyStudent = from student in lstStudent where student.ID ==ID  select student;
            var lstModifyStudent = modifyStudent.ToList();
            if (modifyStudent == null || lstModifyStudent.Count == 0)
            {
                throw new ArgumentNullException("AjaxModifyStudent modifyStudent or modifyStudent.Count is 0 or null");
            }

            return lstModifyStudent.FirstOrDefault();

        }
    }
}
