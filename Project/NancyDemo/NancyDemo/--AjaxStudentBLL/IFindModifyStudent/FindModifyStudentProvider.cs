﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.AjaxStudentBLL
{
    public abstract class FindModifyStudentProvider : IFindModifyStudent
    {

        public virtual viewStudent FindModifyStudent(int ID, List<viewStudent> lstStudent)
        {
            if (ID <= 0||lstStudent == null)
            {
                throw new ArgumentNullException("FindModifyStudentProvider Argument ID or lstStudent is 0 or null");
            }

            return this.FindStudentLogic(ID,lstStudent);
        }

        protected abstract viewStudent FindStudentLogic(int ID, List<viewStudent> lstStudent);
    }
}
