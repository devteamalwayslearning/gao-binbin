﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.AjaxStudentBLL
{
    public interface IFindModifyStudent
    {
        viewStudent FindModifyStudent(int ID, List<viewStudent> lstStudent);  
    }
}
