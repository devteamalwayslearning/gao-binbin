﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.AjaxStudentBLL
{
    public class AjaxDeleteStudent : DeleteStudentProvider
    {
        public static object synLock = new object();
        protected override bool DeleteStudentLogical(int ID,List<viewStudent> lstStudent)
        {
            bool bSucess=false;
            viewStudent student = lstStudent.Find(s=>s.ID == ID);
            if (student ==null)
            {
                return bSucess;
            }

            lock (synLock)
            {
                bSucess =  lstStudent.Remove(student);
            }
            return bSucess;
        }
    }
}
