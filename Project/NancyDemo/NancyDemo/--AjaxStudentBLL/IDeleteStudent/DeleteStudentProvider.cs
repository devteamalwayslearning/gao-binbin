﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.AjaxStudentBLL
{
    public abstract class DeleteStudentProvider : IDeleteStudent
    {
        public virtual bool DeleteStudent(int ID, List<viewStudent> lstStudent)
        {
           if(ID <=0||lstStudent.Count == 0)
           {
               throw new ArgumentException("DeleteStudentProvider ID or lstStudent.Count is 0");
           }

           return this.DeleteStudentLogical(ID, lstStudent);
        }

        protected abstract bool DeleteStudentLogical(int ID, List<viewStudent> lstStudent);
    }
}
