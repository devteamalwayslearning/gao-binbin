﻿using Library;
using Entity;
using Nancy;
using NancyDemo.ConvertModel;
using NancyDemo.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace NancyDemo.Controller
{
    public class TodoModule : NancyModule
    {
 
        private IService<ListItem> _iService;
        private IConvertModel<ListItem, viewListItem> _iConvertModel;
        public TodoModule
        (
            IService<ListItem> iService,
            IConvertModel<ListItem, viewListItem> iConvertModel
        )
        {
            _iService = iService;
            _iConvertModel = iConvertModel;
            //获取所有TaskItem
            Get["/newtodo"] = allList => GetListAll(allList);
            //添加数据
            //Post["/todo"] = addlist => Addlist(addlist);
            //查询数据
            Post["/newtodo/search"] = searchlist => Searchlist(searchlist);
            //修改数据
            Put["/newtodo"] = modifyList => ModifyList(modifyList);
            //删除数据
            Delete["/newtodo"] = deleteList => DeleteList(deleteList);
            //测试代码
            //Get["/todo/{name}"] = allList => GetListSearchName(allList);
        }
        /// <summary>
        /// 测试代码
        /// </summary>
        /// <param name="allList"></param>
        /// <returns></returns>
        public dynamic GetListSearchName(dynamic allList)
        {
            Request.Session["session"] = "seid";
            var lstTaskAll = _iService.GetAllModel();
            lstTaskAll.Add(new ListItem { ItemId=1,Title="xiaohong"});
            var viewLstModel = _iConvertModel.CovertLstViewModel(lstTaskAll);
            var jsonResult = new JsonResult();
            jsonResult.Result = viewLstModel;
            return Response.AsJson(jsonResult);
        }



        /// <summary>
        /// 获取全部数据 
        /// </summary>
        /// <param name="allList"></param>
        /// <returns></returns>
        public dynamic GetListAll(dynamic allList)
        {
            var lstTaskAll = _iService.GetAllModel();
            var viewLstModel = _iConvertModel.CovertLstViewModel(lstTaskAll);
            return View["/Todo", viewLstModel];
        }

        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="addlist"></param>
        /// <returns></returns>
        //public dynamic DeleteList(dynamic deleteList)
        //{
        //    var Title = Request.Form["Title"];
        //    if (string.IsNullOrEmpty(Title))
        //    {
        //        var originlstItem = _iService.GetAllModel();
        //        var originLstViewItem = _iConvertModel.CovertLstViewModel(originlstItem);
        //        return View["/Todo", originLstViewItem];
        //    }
        //    Random random = new Random();
        //    var ID =  random.Next(100);
        //    ListItem taskItem = new ListItem() {
        //        ItemId =ID,
        //        Title = Title
        //    };

        //    _iService.InsertModel(taskItem);
        //    var lstItem = _iService.GetAllModel();
        //    var LstViewItem = _iConvertModel.CovertLstViewModel(lstItem);
        //    return View["/Todo", LstViewItem];
        //}
        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="modifyList"></param>
        /// <returns></returns>
        public dynamic ModifyList(dynamic modifyList)
        {
            var bSuccess = false;
            var jsonResult = new JsonResult();
            var ItemId = Request.Form["ItemId"];
            var Title = Request.Form["Title"];
            if (ItemId == 0 || string.IsNullOrEmpty(Title))
            {
                jsonResult.Result = bSuccess;
                jsonResult.Message = "Parameter Incomplete";
                jsonResult.State = 0;
                return Response.AsJson(jsonResult);
            }

            _iService.SearchAndModify(ItemId, "ItemId", Title, "Title");
            bSuccess = true;
            jsonResult.Result = bSuccess;
            jsonResult.State = 1;
            return Response.AsJson(jsonResult);
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="deleteList"></param>
        /// <returns></returns>
        public dynamic DeleteList(dynamic deleteList)
        {
            var bSuccess = false;
            var jsonResult = new JsonResult();
            var ItemId = Request.Form["ItemId"];
            if (ItemId<=0)
            {
                return Response.AsJson(bSuccess);
            }
            _iService.DeleteModel(ItemId, "ItemId");
            bSuccess = true;
            return Response.AsJson(bSuccess);
        }

        public dynamic Searchlist(dynamic searchlist)
        {
            var Title = Request.Form["Title"];
            if (string.IsNullOrEmpty(Title))
            {
                 var lstItem = _iService.GetAllModel();
                 var LstViewItem = _iConvertModel.CovertLstViewModel(lstItem);
                 return View["/Todo", LstViewItem];
            }

            var lstSearch = _iService.SearchByClos(Title, "Title");
            var LstlstSearch = _iConvertModel.CovertLstViewModel(lstSearch);
            return View["/Todo", LstlstSearch];
        }
    }
}
