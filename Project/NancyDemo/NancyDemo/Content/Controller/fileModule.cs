﻿using Nancy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo.Controller
{
    /// <summary>
    /// 文件上传
    /// </summary>
    public class fileModule: NancyModule
    {
        public fileModule()  
        {
            Get["/uploadfile"] = get =>
            {
                //访问别的控制器下的页面
                //return View["/other/other"];
                return View["/Student/UploadFiles"];
            };

            Get["/deletefile/{fileName}"] = delete =>
            {
                var fileName = delete.fileName;
                var path = AppDomain.CurrentDomain.BaseDirectory;
                path = Path.Combine(path, "UpLoadFile");
                path = Path.Combine(path, fileName);
                if(!File.Exists(path))
                {
                    return View[""];
                }
                //删除文件
                File.Delete(path);
                return View[""];
            };

            Post["/uploadfile"]=postFile=>
            {
                List<HttpFile> lstHttpFile =  Request.Files.ToList();
                if (lstHttpFile == null || lstHttpFile.Count ==0)
                {
                    //跨控制器访问页面
                    return View["/Student/UploadFiles"];
                }
                HttpFile httpFile = lstHttpFile[0];
                var path = AppDomain.CurrentDomain.BaseDirectory;
                path = Path.Combine(path,"UpLoadFile");
                path = Path.Combine(path,httpFile.Name);
                var fileStream = new FileStream(path, FileMode.Create);
                byte[] btRead = new byte[1024*1024*5];
                //获取文件字节流
                Stream stream = httpFile.Value;
                MemoryStream memoryStream = new MemoryStream();
                byte[] btReadNet = new byte[1024 * 1024 * 5];
                int nCountNet = 0;
                while ((nCountNet = httpFile.Value.Read(btReadNet, 0, btReadNet.Length)) > 0)
                {
                    memoryStream.Write(btReadNet, 0, nCountNet);
                }
                memoryStream.Flush();
                memoryStream.Seek(0,SeekOrigin.Begin);
                int nCount = 0;
                while ((nCount = memoryStream.Read(btRead, 0, btRead.Length)) > 0)
                {
                    fileStream.Write(btRead, 0, nCount);
                }
                fileStream.Flush();
                fileStream.Close();
                return View["/Student/UploadFiles"];
            };
           
        }
    }
}
