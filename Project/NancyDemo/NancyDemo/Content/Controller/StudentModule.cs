﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//绑定命名空间
using Nancy.ModelBinding;
using NancyDemo.AjaxStudentBLL;
using Library;
using Entity;
using NancyDemo.ConvertModel;
namespace NancyDemo.Controller
{
    public class StudentModule : NancyModule
    {
        //和Nject框架一样
        //ISearchModifyStudent _iSearchStudent;
        //IAddStudent _iAddStudent;
        //IFindModifyStudent _iFindModifyStudent;
        //IDeleteStudent _iDeleteStudent;
        //ISearchStudent _iAjaxSearchStudent;
        IStudentService _iStudentService;
        IConvertModel<Student, viewStudent>  _iConvertStudentModel;
        //存储 newbitbuket
        //public static List<viewStudent> lstnewStudent = new List<viewStudent>();
        public StudentModule(
            //ISearchModifyStudent iSearchStudent, 
            //IAddStudent iAddStudent,
            //IFindModifyStudent iFindModifyStudent,
            //IDeleteStudent iDeleteStudent,
            //ISearchStudent iAjaxSearchStudent,
            IStudentService iStudentService,
            IConvertModel<Student, viewStudent>  iConvertStudentModel)
        
        {
            //_iSearchStudent = iSearchStudent;
            //_iAddStudent = iAddStudent;
            //_iFindModifyStudent = iFindModifyStudent;
            //_iDeleteStudent = iDeleteStudent;
            //_iAjaxSearchStudent = iAjaxSearchStudent;
            _iStudentService = iStudentService;
            _iConvertStudentModel = iConvertStudentModel;
            Get["/student/show/"] = getData =>
            {
                List<Student> lstStudent = _iStudentService.GetAllStudent();
                var lstviewStudent= _iConvertStudentModel.CovertLstViewModel(lstStudent);
                return View["/ShowStudent", lstviewStudent];
            };

            Get["/student/show/{studentID}"] = get =>
            {
                var studentID = get.studentID;
                int studentid = 0;
                List<Student> lstStudent = _iStudentService.GetAllStudent();
                if (!int.TryParse(studentID, out studentid))
                {
                    return View["/ShowStudent", lstStudent];
                }

                var modifyStudent = _iStudentService.FindModifyStudent(studentid);
                var viewmodifyStudent =  _iConvertStudentModel.ConvertToViewModel(modifyStudent);
                return View["Modify", viewmodifyStudent];
            };
            //删除学生
            Post["/student/delete"] = postJson =>
            {
                var StudentID = Request.Form["StudentID"];
                int studentId = 0;
                bool bSuess = false;
                if (!int.TryParse(StudentID,out studentId))
                {
                    return Response.AsJson(bSuess);
                }

                bSuess = _iStudentService.DeleteStudent(studentId);
                return Response.AsJson(bSuess);
            };

            Post["/student/modifyPost"] = modify =>
            {
                //模型绑定 
                Student modifyStudent = this.Bind<Student>(); //model binding!
                var lstStudent = _iStudentService.SearchAndModify(modifyStudent);
                var lstviewStudent = _iConvertStudentModel.CovertLstViewModel(lstStudent);
                return View["/ShowStudent", lstviewStudent];
            };

            //添加学生
            Post["/student/addstudent"] = add =>
            {
                Student addStudent = this.Bind<Student>(); //model binding!
                if (addStudent == null || string.IsNullOrEmpty(addStudent.Name))
                {
                    List<Student> lstStudent = _iStudentService.GetAllStudent();
                    var viewlstStudent = _iConvertStudentModel.CovertLstViewModel(lstStudent);
                    return View["/ShowStudent", viewlstStudent];
                }
                Random random = new Random();
                addStudent.ID= random.Next(100);
                _iStudentService.InsertStudent(addStudent);
                List<Student> lstInsertStudent = _iStudentService.GetAllStudent();
                var viewlstInsertStudent = _iConvertStudentModel.CovertLstViewModel(lstInsertStudent);
                return View["/ShowStudent", viewlstInsertStudent];
            };

            //查询学生
            Post["/student/search"] = search =>
            {
                var form = Request.Form;
                var SearchName = form["Search"];
                if (string.IsNullOrEmpty(SearchName))
                {
                    return View["/ShowStudent", new List<viewStudent>()];
                }

                List<Student> lstSearchStudent = _iStudentService.SearchByName(SearchName);
                var viewlstInsertStudent = _iConvertStudentModel.CovertLstViewModel(lstSearchStudent);
                return View["/ShowStudent", viewlstInsertStudent];
            };
        }
 
    }
}
