﻿
 
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace IBLL
{ 
	public partial interface IBLLSession
	{
		IBLL_ControlAction  iBLL_ControlAction{get;set;}	      
		IBLL_ControlActionRole  iBLL_ControlActionRole{get;set;}	      
		IBLL_CRole  iBLL_CRole{get;set;}	      
		IBLL_CustormUser  iBLL_CustormUser{get;set;}	      
		IBLL_EF_Student  iBLL_EF_Student{get;set;}	      
		IBLL_EF_Teacher  iBLL_EF_Teacher{get;set;}	      
		IBLL_UserRole  iBLL_UserRole{get;set;}	      
	}
}




      
