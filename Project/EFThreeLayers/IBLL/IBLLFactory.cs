﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBLL
{
    public interface IBLLFactory<T> where T : class,new()
    {
        IBLLBase<T> GetFactory();
    }
}
