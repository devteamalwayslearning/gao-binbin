﻿
  
using IDAL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace IDAL
{ 
	public partial interface IDBSession
	{
		IDal_ControlAction  iDal_ControlAction{get;set;}	      
		IDal_ControlActionRole  iDal_ControlActionRole{get;set;}	      
		IDal_CRole  iDal_CRole{get;set;}	      
		IDal_CustormUser  iDal_CustormUser{get;set;}	      
		IDal_EF_Student  iDal_EF_Student{get;set;}	      
		IDal_EF_Teacher  iDal_EF_Teacher{get;set;}	      
		IDal_UserRole  iDal_UserRole{get;set;}	      
	}
}




      
