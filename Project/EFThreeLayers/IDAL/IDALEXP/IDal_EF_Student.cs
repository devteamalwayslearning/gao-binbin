﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDAL
{
    public partial interface IDal_EF_Student
    {
        void Hello(string a);
        bool HasStudent(string Name);
        void SqlQery();
        List<EF_Student> Query();
        List<EF_Student> QueryOnlyShow();
        void TransactionScope(EF_Student eF_Student, EF_Teacher eF_Teacher);
    }
}
