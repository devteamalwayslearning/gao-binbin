﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstModel
{
    /// <summary>
    /// code first
    /// </summary>
    [Table("Code_EF_Student")]
    public class Code_EF_Student
    {
        //主键符号
        [Key]
        public int ID { set; get; }
        //必须
        [Required]
        [MaxLength(10)]
        public string Name { set; get; }

        public int Add { set; get; }
        //[ForeignKey("Code_EF_Teacher")]
        //public int TeacherId { set; get; }

    }
}
