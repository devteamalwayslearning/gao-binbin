﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstModel
{
    [Table("Code_EF_Teacher")]
    public class Code_EF_Teacher
    {
        //主键符号
        [Key]
        public int ID { set; get; }
        //非空
        [Required]
        //长度标志
        [MaxLength(10)]
        public string Name { set; get; }
        public int sID { set; get; }

     
    }
}
