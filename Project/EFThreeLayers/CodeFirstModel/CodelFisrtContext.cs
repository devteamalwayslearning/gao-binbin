﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstModel
{
    public class CodelFisrtContext : DbContext
    {
        public CodelFisrtContext()
            : base("name = CodeFirstDb")
         {
         }
        /// <summary>
        /// 数据迁移必须要添加
        /// </summary>
        /// <param name="connectionName"></param>
        public CodelFisrtContext(string connectionName)
            : base(connectionName)
        {
        }
        //Enable-Migrations -ProjectName  CodeFirstModel  在程序包管理器控制台
        public DbSet<Code_EF_Student> Code_EF_StudentContext { set; get; }
        public DbSet<Code_EF_Teacher> Code_EF_TeacherContext { set; get; }
    }
}
