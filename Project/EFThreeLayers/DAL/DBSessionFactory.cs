﻿using IDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DBSessionFactory : IDBSessionFactory
    {
        public IDBSession GetDBSession()
        {
            IDBSession iDBSession = CallContext.GetData(typeof(DBSessionFactory).FullName) as IDBSession;
            if (iDBSession == null)
            {
                iDBSession = new DBSession();
                CallContext.SetData(typeof(DBSessionFactory).FullName, iDBSession);
            }
            return iDBSession;
        }
    }
}
