﻿using IDAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    /// <summary>
    /// 因为将DbSet传出来了,所以就可以在业务层直接做延迟加载，不需要将SaveChange()放到上面
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseDAL<T> : IBaseDAL<T> where T : class,new()
    {

        protected DbContext _db = null;
        protected DbContext db =null;
       
        //这个用于复杂业务处理
        private IDbSet<T> _entities = null;

        private IDbContextFactory iDbContextFactory = null;

        protected BaseDAL()
        {
            if (db == null)
            {
                iDbContextFactory = new DbContextFactory();
                db = iDbContextFactory.GetDbContext();
            }
            _entities = db.Set<T>();
        }
     
         public IQueryable<T> Entities
         {
             get
             {
                 return _entities;
             }
         }

        public T Add(T model)
        {
            db.Set<T>().Add(model);
            db.SaveChanges();
            return model;
        }

        public bool BulkAdd(List<T> lstModel)
        {
            int Count = 0;
            lstModel.ForEach(u=>{
                 db.Set<T>().Add(u);
                 Count++;
                });
            int Result= db.SaveChanges();
            if (Count != lstModel.Count || Result < 0)
            {
                return false;
            }

            return true;
        }


        public List<T> Query(Expression<Func<T,bool>> whereExp)
        {
             List<T> lstModel = db.Set<T>().Where(whereExp).ToList();
             return lstModel;
        }

        public List<T> Query<Key>(Expression<Func<T, bool>> whereExp, Expression<Func<T, Key>> orderExp, int pageIndex, int pageSize)
        {
            List<T> lstModel = db.Set<T>().Where(whereExp).OrderBy(orderExp).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            return lstModel;
        }

        public int Modify(Expression<Func<T, bool>> whereExp, string Name,string propertyName)
        {
            Type t = typeof(T);
            List<T> lstModel = db.Set<T>().Where(whereExp).ToList();
            lstModel.ForEach(u =>{
                PropertyInfo p = t.GetProperty(propertyName);
                p.SetValue(u,Name);
                });
            return db.SaveChanges();
        }


        public int Modify(T model,string[] clos)
        {
            DbEntityEntry<T> entry =  db.Entry<T>(model);
            //状态
            entry.State = EntityState.Unchanged;
            List<string> lstClos = new List<string>(clos);
            lstClos.ForEach(s =>
                {
                    entry.Property(s).IsModified = true;
                });
            return db.SaveChanges();
        }

        public int Delete(T model)
        {
            db.Set<T>().Attach(model);
            db.Set<T>().Remove(model);
            return db.SaveChanges();
        }





    }
}
