﻿using IDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public partial class DBSession : IDBSession
    {
        private IDbContextFactory iDbContextFactory = null;
        public DBSession()
        {
            iDbContextFactory = new DbContextFactory();
        }


        public int SaveChange()
        {
            return iDbContextFactory.GetDbContext().SaveChanges();
        }
    }
}
