﻿using IDAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DbContextFactory : IDbContextFactory
    {
        public System.Data.Entity.DbContext GetDbContext()
        {

            DbContext dbContext = CallContext.GetData(typeof(DbContextFactory).FullName) as DbContext;
            if (dbContext == null)
            {
                dbContext = new MyDBEntities();
                CallContext.SetData(typeof(DbContextFactory).FullName, dbContext);
            }
            return dbContext;
        }
    }
}
