﻿using DAL;
using IDAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
namespace DAL
{
    public partial class Dal_EF_Student : BaseDAL<EF_Student>, IDal_EF_Student 
    {
        public void Hello(string a)
        {
            Console.Write("你好");
        }
        /// <summary>
        /// 添加存储过程(无结果集返回)
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public bool HasStudent(string Name)
        {
            object obj = null;
            SqlParameter sqlParameter = new SqlParameter("@IsRight", SqlDbType.Int);
            sqlParameter.Direction = ParameterDirection.Output;
            this.db.Database.ExecuteSqlCommand("EXEC hasStuProc33 @Name,@IsRight out", new SqlParameter("@Name", Name), sqlParameter);
            //var q= this.db.Database.ExecuteSqlCommand("select *,* from EF_Student");
            obj = sqlParameter.Value;

            if ((int)obj == 111)
            {
                return true;
            }

            return false;
        }
        //存储过程应用，表返回的存储过程(有结果集返回)
        public void SqlQery()
        {
            //直接源生sql操作数据库
            //var q = this.db.Database.SqlQuery<EF_Student>("select * from  EF_Student").ToList();
            object obj = null;
            SqlParameter sqlParameter = new SqlParameter("@IsRight", SqlDbType.Int);
            sqlParameter.Direction = ParameterDirection.Output;

            var q = this.db.Database.SqlQuery<EF_Student>("EXEC hasStuProc66 @Name,@IsRight out", new SqlParameter("@Name", "大名d"), sqlParameter).ToList();
            obj = sqlParameter.Value;


        }
        //edmx EF操作存储过程
        /// <summary>
        /// ObjectParameter参数对应输出类型时，不必指定类似Output等
        //ObjectParameter的命名空间：using System.Data.Objects;
        //ObjectParameter参数的Name是对应存储过程参数字符串去掉@符号，                                                                     
        //例如存储过程参数‘@Count int output‘，对应Name为”Count“,注意不区分大小写
        /// </summary>
        //public void defaultProc()
        //{                                                                                                                      //名字和以前一致
        //    System.Data.Entity.Core.Objects.ObjectParameter sqlParameter = new System.Data.Entity.Core.Objects.ObjectParameter("IsRight", SqlDbType.Int);

        //    this.db.hasStuProc33("大名", sqlParameter);

        //}

        /// <summary>
        /// 验证延迟加载
        /// </summary>
        /// <returns></returns>
        public List<EF_Student> Query()
        {
            var query = db.Set<EF_Student>().Where(u => 1 == 1);
            var numerable = query.AsEnumerable();
            var q2 = numerable.Take(2);
            var q3 = q2.AsQueryable();
            List<EF_Student> lstModel = q3.ToList();
            var query1 = db.Set<EF_Student>().Where(u => 1 == 1).Take(2);
            List<EF_Student> lstModel1 = query1.ToList();
            return lstModel;
        }


        public List<EF_Student> QueryOnlyShow()
        {
            //只用来查询不能修改删除
            var lstModel = db.Set<EF_Student>().Where(u => 1 == 1).AsNoTracking().ToList();
            lstModel[0].Name = "大大大";
            db.SaveChanges();
            return lstModel;
        }
        /// <summary>
        /// 事物应用
        /// </summary>
        /// <param name="eF_Student"></param>
        /// <param name="eF_Teacher"></param>
        public void TransactionScope(EF_Student eF_Student, EF_Teacher eF_Teacher)
        {
            //通用事物
            using (TransactionScope scope = new TransactionScope())
            {

                db.Set<EF_Student>().Add(eF_Student);
                db.Set<EF_Teacher>().Add(eF_Teacher);
                db.SaveChanges();
                scope.Complete();
            }
            //EF包含事物
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Set<EF_Student>().Add(eF_Student);
                    db.Set<EF_Teacher>().Add(eF_Teacher);
                    db.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                }

            }
        }
    }
}

