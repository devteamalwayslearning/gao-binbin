﻿
 
using IDAL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace DAL
{ 
	public partial class DBSession:IDBSession
	{
		IDal_ControlAction  _iDal_ControlAction{get;set;}	
		public IDal_ControlAction  iDal_ControlAction
		{
			get
			{
					if(_iDal_ControlAction == null)
					{
						_iDal_ControlAction = new Dal_ControlAction();
					}
					return _iDal_ControlAction;
			}
			set
			{
					_iDal_ControlAction = value;
			}
		}   
		IDal_ControlActionRole  _iDal_ControlActionRole{get;set;}	
		public IDal_ControlActionRole  iDal_ControlActionRole
		{
			get
			{
					if(_iDal_ControlActionRole == null)
					{
						_iDal_ControlActionRole = new Dal_ControlActionRole();
					}
					return _iDal_ControlActionRole;
			}
			set
			{
					_iDal_ControlActionRole = value;
			}
		}   
		IDal_CRole  _iDal_CRole{get;set;}	
		public IDal_CRole  iDal_CRole
		{
			get
			{
					if(_iDal_CRole == null)
					{
						_iDal_CRole = new Dal_CRole();
					}
					return _iDal_CRole;
			}
			set
			{
					_iDal_CRole = value;
			}
		}   
		IDal_CustormUser  _iDal_CustormUser{get;set;}	
		public IDal_CustormUser  iDal_CustormUser
		{
			get
			{
					if(_iDal_CustormUser == null)
					{
						_iDal_CustormUser = new Dal_CustormUser();
					}
					return _iDal_CustormUser;
			}
			set
			{
					_iDal_CustormUser = value;
			}
		}   
		IDal_EF_Student  _iDal_EF_Student{get;set;}	
		public IDal_EF_Student  iDal_EF_Student
		{
			get
			{
					if(_iDal_EF_Student == null)
					{
						_iDal_EF_Student = new Dal_EF_Student();
					}
					return _iDal_EF_Student;
			}
			set
			{
					_iDal_EF_Student = value;
			}
		}   
		IDal_EF_Teacher  _iDal_EF_Teacher{get;set;}	
		public IDal_EF_Teacher  iDal_EF_Teacher
		{
			get
			{
					if(_iDal_EF_Teacher == null)
					{
						_iDal_EF_Teacher = new Dal_EF_Teacher();
					}
					return _iDal_EF_Teacher;
			}
			set
			{
					_iDal_EF_Teacher = value;
			}
		}   
		IDal_UserRole  _iDal_UserRole{get;set;}	
		public IDal_UserRole  iDal_UserRole
		{
			get
			{
					if(_iDal_UserRole == null)
					{
						_iDal_UserRole = new Dal_UserRole();
					}
					return _iDal_UserRole;
			}
			set
			{
					_iDal_UserRole = value;
			}
		}   
	}
}




      
