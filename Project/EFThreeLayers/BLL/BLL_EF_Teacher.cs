using BLL;
using IBLL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace BLL
{ 
	public partial class BLL_EF_Teacher : BaseBLL<EF_Teacher>,IBLL_EF_Teacher  
	{

	     protected override void GetDAL()
		 {
		     this.baseDAL = iDBSession.iDal_EF_Teacher;
		 }
	}
}
