﻿using DAL;
using IDAL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class BLL_EF_Student : BaseBLL<EF_Student>
    {
       

        //protected override void GetDAL()
        //{
        //    dal_EF_Student = (IDal_EF_Student)this.baseDAL;
        //    //this.baseDAL = dal_EF_Student;
        //}
        
        public void Hello(string a)
        {
            this.iDBSession.iDal_EF_Student.Hello(a);
        }

        public bool HasStudent(string Name)
        {
            return this.iDBSession.iDal_EF_Student.HasStudent(Name);
        }

        public void SqlQery()
        {
            //dal_EF_Student.SqlQery();
            //dal_EF_Student.defaultProc();
        }  

        public void QeryCheck()  
        {
            //dal_EF_Student.SqlQery();
            this.iDBSession.iDal_EF_Student.QueryOnlyShow();
        }

        public void TransactionScope(EF_Student eF_Student, EF_Teacher eF_Teacher)
        {
            this.iDBSession.iDal_EF_Student.TransactionScope(eF_Student, eF_Teacher);
        }
        /// <summary>
        /// 将DBSet传出来应用
        /// </summary>
        public void Table()
        {
            //IList<EF_Student> Ilst = dal_EF_Student.Entities.Where(s => s.Name == "lixue").ToList();
            //var q = dal_EF_Student.Entities.First(s => s.Name == "lixue");

        }

    }
}

