﻿using IBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class BLLFactory<T> : IBLLFactory<T> where T : class,new ()
    {
        private List<Type> lstType = null;
        public IBLLBase<T> GetFactory()
        {
            if (lstType == null)
            {
                lstType = Assembly.LoadFrom(@"BLL.dll").GetTypes().ToList();
            }

            Type typeT = typeof(T);
            List<Type> lstFind = lstType.Where(s =>
            {
                if (s.BaseType == null)
                {
                    return false;
                }
                if (s.BaseType.GenericTypeArguments.Length == 0)
                {
                    return false;
                }
                if (s.BaseType.GenericTypeArguments[0] != typeT)
                {
                    return false;
                }
                if (!s.BaseType.Name.Contains("BaseBLL"))
                {
                    return false;
                }
                return true;
            }).ToList();
            BaseBLL<T> baseBLL = Activator.CreateInstance(lstFind[0]) as BaseBLL<T>;
            return baseBLL;
        }
    }
}
