using BLL;
using IBLL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace BLL
{ 
	public partial class BLL_UserRole : BaseBLL<UserRole>,IBLL_UserRole  
	{

	     protected override void GetDAL()
		 {
		     this.baseDAL = iDBSession.iDal_UserRole;
		 }
	}
}
