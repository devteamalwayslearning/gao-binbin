﻿
 
using IBLL;
using BLL;
using IDAL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace DAL
{ 
	public partial class BLLSession:IBLLSession
	{
		IBLL_ControlAction  _iBLL_ControlAction{get;set;}	
		public IBLL_ControlAction  iBLL_ControlAction
		{
			get
			{
					if(_iBLL_ControlAction == null)
					{
						_iBLL_ControlAction = new BLL_ControlAction();
					}
					return _iBLL_ControlAction;
			}
			set
			{
					_iBLL_ControlAction = value;
			}
		}   
		IBLL_ControlActionRole  _iBLL_ControlActionRole{get;set;}	
		public IBLL_ControlActionRole  iBLL_ControlActionRole
		{
			get
			{
					if(_iBLL_ControlActionRole == null)
					{
						_iBLL_ControlActionRole = new BLL_ControlActionRole();
					}
					return _iBLL_ControlActionRole;
			}
			set
			{
					_iBLL_ControlActionRole = value;
			}
		}   
		IBLL_CRole  _iBLL_CRole{get;set;}	
		public IBLL_CRole  iBLL_CRole
		{
			get
			{
					if(_iBLL_CRole == null)
					{
						_iBLL_CRole = new BLL_CRole();
					}
					return _iBLL_CRole;
			}
			set
			{
					_iBLL_CRole = value;
			}
		}   
		IBLL_CustormUser  _iBLL_CustormUser{get;set;}	
		public IBLL_CustormUser  iBLL_CustormUser
		{
			get
			{
					if(_iBLL_CustormUser == null)
					{
						_iBLL_CustormUser = new BLL_CustormUser();
					}
					return _iBLL_CustormUser;
			}
			set
			{
					_iBLL_CustormUser = value;
			}
		}   
		IBLL_EF_Student  _iBLL_EF_Student{get;set;}	
		public IBLL_EF_Student  iBLL_EF_Student
		{
			get
			{
					if(_iBLL_EF_Student == null)
					{
						_iBLL_EF_Student = new BLL_EF_Student();
					}
					return _iBLL_EF_Student;
			}
			set
			{
					_iBLL_EF_Student = value;
			}
		}   
		IBLL_EF_Teacher  _iBLL_EF_Teacher{get;set;}	
		public IBLL_EF_Teacher  iBLL_EF_Teacher
		{
			get
			{
					if(_iBLL_EF_Teacher == null)
					{
						_iBLL_EF_Teacher = new BLL_EF_Teacher();
					}
					return _iBLL_EF_Teacher;
			}
			set
			{
					_iBLL_EF_Teacher = value;
			}
		}   
		IBLL_UserRole  _iBLL_UserRole{get;set;}	
		public IBLL_UserRole  iBLL_UserRole
		{
			get
			{
					if(_iBLL_UserRole == null)
					{
						_iBLL_UserRole = new BLL_UserRole();
					}
					return _iBLL_UserRole;
			}
			set
			{
					_iBLL_UserRole = value;
			}
		}   
	}
}




      
