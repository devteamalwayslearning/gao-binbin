﻿using IBLL;
using IDAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public abstract class BaseBLL<T> : IBLLBase<T> where T : class,new()
    {
        protected IBaseDAL<T> baseDAL = null;
        //利用partial页来做3层
        private IDBSession _iDBSession = null;

        protected IDBSession iDBSession
        {
            get
            {
                if (_iDBSession == null)
                {
                  
                    string dllPath = AppDomain.CurrentDomain.BaseDirectory + @"bin\DAL.dll";
                    Type type = Assembly.LoadFrom(dllPath).GetType("DAL.DBSessionFactory");
                    IDBSessionFactory iDBSessionFactory = (IDBSessionFactory)Activator.CreateInstance(type);
                    _iDBSession = iDBSessionFactory.GetDBSession();
                }
                return _iDBSession;
            }
        }

        private List<Type> lstType;

        private void BaseTypeFactory()
        {
            //替代反射方法
            if (lstType == null)
            {
                lstType = Assembly.LoadFrom(@"DAL.dll").GetTypes().ToList();
            }

            Type typeT = typeof(T);
            List<Type> lstFind = lstType.Where(s =>
            {
                if (s.BaseType == null)
                {
                    return false;
                }
                if (s.BaseType.GenericTypeArguments.Length == 0)
                {
                    return false;
                }
                if (s.BaseType.GenericTypeArguments[0] != typeT)
                {
                    return false;
                }
                if (!s.BaseType.Name.Contains("BaseDAL"))
                {
                    return false;
                }
                return true;
            }).ToList();

            baseDAL = Activator.CreateInstance(lstFind[0]) as IBaseDAL<T>;
        }

   
        public BaseBLL()
        {
            //BaseTypeFactory();
            this.GetDAL();
        }

        protected abstract void GetDAL();

        public T Add(T model)
        {
            return baseDAL.Add(model);
        }

        public bool BulkAdd(List<T> lstModel)
        {
           return baseDAL.BulkAdd(lstModel);
        }
  
        public List<T> Query(Expression<Func<T, bool>> whereExp)
        {
            return baseDAL.Query(whereExp);
        }

        public List<T> Query<Key>(Expression<Func<T, bool>> whereExp, Expression<Func<T, Key>> orderExp, int pageIndex, int pageSize)
        {
             return baseDAL.Query<Key>(whereExp,orderExp,pageIndex,pageSize);
        }

        public int Modify(Expression<Func<T, bool>> whereExp, string Name, string propertyName)
        {
            return baseDAL.Modify(whereExp,Name,propertyName);
        }


        public int Modify(T model, string[] clos)
        {
            return baseDAL.Modify(model,clos);
        }

        public int Delete(T model)
        {
            return baseDAL.Delete(model);
        }
    }
}
