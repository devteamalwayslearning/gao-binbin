﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IBLLFactory
{
    public interface IBLL<T>where T:class
    {
        T Add(T model);

        bool BulkAdd(List<T> lstModel);

        List<T> Query(Expression<Func<T, bool>> whereExp);

        List<T> Query<Key>(Expression<Func<T, bool>> whereExp, Expression<Func<T, Key>> orderExp, int pageIndex, int pageSize);

        int Modify(Expression<Func<T, bool>> whereExp, string Name, string propertyName);

        int Modify(T model, string[] clos);

        int Delete(T model);
     
    }
}
