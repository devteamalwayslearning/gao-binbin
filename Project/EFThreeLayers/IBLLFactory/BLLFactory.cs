﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IBLLFactory
{
    public class BLLFactory<T> : IBLLFactorycs<T> where T : class,new()
    {
        #region  单例模型
        private static BLLFactory<T> _instance = null;
        // Creates an syn object.
        private static readonly object SynObject = new object();

        BLLFactory()
        {
        }

        public static BLLFactory<T> Instance
        {
            get
            {
                // Double-Checked Locking
                if (null == _instance)
                {
                    lock (SynObject)
                    {
                        if (null == _instance)
                        {
                            _instance = new BLLFactory<T>();
                        }
                    }
                }
                return _instance;
            }
        }
        #endregion

        protected List<Type> bLLType = null;

        private Type typeAbstract;
        public T CreatBLL()
        {
            if (bLLType == null)
            {
                bLLType = Assembly.LoadFrom("BLL.dll").GetTypes().ToList();
            }

            Type typeT = typeof(T);

            typeAbstract = bLLType.Find(s=>s.IsAbstract ==true);

            List<Type> findType = bLLType.Where(s =>
                                                    {
                                                        if (s.BaseType != typeAbstract)
                                                        {
                                                            return false;
                                                        }
                                                        Type argumType = s.GenericTypeArguments[0];
                                                        if (argumType != typeT)
                                                        {
                                                            return false;
                                                        }
                                                        return true;
                                                    }).ToList();

            if (findType == null || findType.Count!=1)
            {
                throw new ArgumentException();
            }

            T t = Activator.CreateInstance(findType[0]) as T;

            return t;
        }

    }
}
