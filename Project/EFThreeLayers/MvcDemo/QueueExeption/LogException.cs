﻿using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;

namespace MvcDemo.QueueExeption
{
    /// <summary>
    /// 整体处理Exception
    /// </summary>
    public class LogException
    {
        public static Queue queue = new Queue();

        public static void SartWrite()
        {
            Thread thread = new Thread(()=>{
                while(true)
                {
                    if(queue.Count==0)
                    {
                       Thread.Sleep(1000);
                       continue;
                    }

                    Exception exception = queue.Dequeue() as Exception;
                    if (exception == null || string.IsNullOrEmpty(exception.Message))
                    {
                        continue;
                    }
                                                     //log文件错误名称
                    ILog log = log4net.LogManager.GetLogger("Exption");
                    log.Error("error", new Exception("发生了一个异常"));
                } });
            thread.Start();
        }

    }
}