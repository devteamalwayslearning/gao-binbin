﻿using IBLL;
using IOC;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TransferModel;

namespace MvcDemo.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        [HttpGet]
        public ActionResult Index(string Age, string Name, string link)
        {
         
            //路由传值必须在post方式下
            string Url = Request.Url.ToString();
            string Ageform = Request.Form["Age"];
            string Name1 =  Request.Form["Name"];

            SelectListItem SelectListItem = new SelectListItem { Text  = "1", Value="1",Selected =true};
            List<SelectListItem> lstSelectListItem = new List<System.Web.Mvc.SelectListItem>();
            lstSelectListItem.Add(SelectListItem);
            ViewData["select"] = lstSelectListItem;


            //throw new ArgumentNullException("dd");
            //获取行为名称
            string str = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();
     
            return Redirect("Index");
        }
        [HttpPost]
        public ActionResult Index()
        {
            HttpCookie httpCookie = new HttpCookie("cook1");
            httpCookie.Value = "cookvalue";
   
            return View();
        }



        [HttpGet]
        public ActionResult  Partial(string Name)
        {

            if(ModelState.IsValid)
            {
            }

          
            string o = Name;
            return PartialView();
        }

        [HttpGet]
        public ActionResult login()
        {
         
            HttpCookie httpCookie = Request.Cookies["loach"];
            if (httpCookie == null)
            {
                return View();
            }
            string Value = httpCookie.Value;
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(Value);
            if (ticket == null)
            {
                return View();
            }
            string userData = ticket.UserData;

            if (string.IsNullOrEmpty(userData))
            {
                return View();
            }

            LoginModel loginModel = JsonConvert.DeserializeObject<LoginModel>(userData);
            if (loginModel == null||string.IsNullOrEmpty(loginModel.Name)||string.IsNullOrEmpty(loginModel.Password))
            {
                return View();
            }

            CustormUser custormUser = OperateContext.iBLLSession.iBLL_CustormUser.Query(s => s.UserName == loginModel.Name && s.Passord == loginModel.Password).First();
            if (custormUser == null)
            {
                return View();
            }

            //IBLLBase<EF_Student> bLL_EF_Student = OperateFactory.GetObject<IBLL_EF_Student>("bll");
            //HttpCookie loginHttpCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            //if (loginHttpCookie != null)
            //{
            //    FormsAuthenticationTicket loginTicket = FormsAuthentication.Decrypt(loginHttpCookie.Value);
            //    string strName = loginTicket.Name;
            //    IBLLBase<EF_Student> bLL_EF_Student = OperateFactory.GetObject<IBLL_EF_Student>("bll");
            //    List<EF_Student> lstEF_Student = bLL_EF_Student.Query(s=>s.Name ==strName);
            //    //if (lstEF_Student == null || lstEF_Student.Count==0)
            //    //{
            //    //    return View();
            //    //}
            //    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
            //    2, loginTicket.Name+"$$$$$$$", DateTime.Now, DateTime.Now.AddMinutes(2), true, "");
            //    string strCookies = FormsAuthentication.Encrypt(ticket);
            //    HttpCookie httpCookie = new HttpCookie(FormsAuthentication.FormsCookieName);
            //    httpCookie.Value = strCookies;
            //    httpCookie.Expires = DateTime.Now.AddMinutes(4);
            //    httpCookie.HttpOnly = true;
            //    Response.Cookies.Set(httpCookie);
            //    return Redirect("~/Home/Link");
            //}


            return View();
        }
       
        [HttpPost]
        public ActionResult login(FormCollection form)
        {
            string Name = form["Name"];
            string Password = form["Password"];
            if (string.IsNullOrEmpty(Name) || string.IsNullOrEmpty(Password))
            {
                return View();
            }
            //md5加密
            string Md5Password = FormsAuthentication.HashPasswordForStoringInConfigFile(Password, "MD5");
            //获取客户
            CustormUser custormUser = OperateContext.iBLLSession.iBLL_CustormUser.Query(s => s.UserName == Name && s.Passord == Md5Password).FirstOrDefault();
            if (custormUser == null)
            {
                return View();
            }
            
            //获取角色对象
            List<int> lstUserRoleID = OperateContext.iBLLSession.iBLL_UserRole.Query(s => s.UserID == custormUser.UserID).Select(s => s.RoleID).ToList();

            List<int> lstRoleId = OperateContext.iBLLSession.iBLL_CRole.Query(s => lstUserRoleID.Contains(s.ID)).Select(s => s.ID).ToList();

            string JsonlstRoleId = JsonConvert.SerializeObject(lstRoleId);

            Session["RoleID"] = JsonlstRoleId;

            if (!string.IsNullOrEmpty(Request.Form["Remmber"]))
            {
                LoginModel loginModel = new LoginModel(){Name=Name,Password =Md5Password};
                string jsonloginModel = JsonConvert.SerializeObject(loginModel);
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,"1",DateTime.Now,DateTime.Now.AddMinutes(20),false,jsonloginModel);
                string value =  FormsAuthentication.Encrypt(ticket);
                HttpCookie httpCookie = new HttpCookie("loach", value);
                httpCookie.Expires = DateTime.Now.AddMinutes(20);
                Response.Cookies.Add(httpCookie);
            }
            
            //IBLLBase<EF_Student> bLL_EF_Student = OperateFactory.GetObject<IBLL_EF_Student>("bll");
            //bLL_EF_Student.Add(new EF_Student() { Name ="MVC"});

            ////BLL_EF_Teacher bLL_EF_Teacher = new BLL_EF_Teacher();
            //var q = Request;
            //// 2. 创建一个FormsAuthenticationTicket，它包含登录名以及额外的用户数据。
            //FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
            //    2, "Namedd", DateTime.Now, DateTime.Now.AddMinutes(2), true,Name);
            //string strCookies = FormsAuthentication.Encrypt(ticket);
            //HttpCookie httpCookie = new HttpCookie(FormsAuthentication.FormsCookieName);
            //httpCookie.Value = strCookies;
            //httpCookie.Expires = DateTime.Now.AddMinutes(4);
            //httpCookie.HttpOnly = true;
            //Response.Cookies.Add(httpCookie);
            return Redirect("~/Home/Link");
        }

         [MyAuthorizeAttribute]
        public ActionResult Link(string Name)
        {
            bool b = Request.IsAuthenticated;

           // HttpCookie ht =   Request.Cookies[FormsAuthentication.FormsCookieName];

           // FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(ht.Value);
           // string strUserDate = ticket.UserData;
           //var user= Request.RequestContext.HttpContext.User;

           //退出登录
           //FormsAuthentication.SignOut();

            return View();
        }

    }
}
