﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace MvcDemo.Controllers
{
    public class TestCacheController : Controller
    {
        //
        // GET: /TestCache/

        public ActionResult Index(string Name)
        {
            if(!string.IsNullOrEmpty(Name))
            {
                //数据缓存和Memcach差不多，只不过一个是分布式一个是单机的
                //Insert可以改写具有相同建的数值:绝对过期时间
                 HttpRuntime.Cache.Insert("Key1", "Value 1", null, DateTime.Now.AddSeconds(60),Cache.NoSlidingExpiration);
                //Insert可以改写具有相同建的数值:相对过期时间
                 HttpRuntime.Cache.Insert("Key1", "Value 1", null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 1, 0));
                 //实例化数据缓存必须这么做
                 //protected static volatile System.Web.Caching.Cache webCache = System.Web.HttpRuntime.Cache;
            }

            if (HttpRuntime.Cache["Key1"] == null)
            {
                ViewData["Cache"] = "没有缓存";
            }

            object objCache = HttpRuntime.Cache["Key1"];
           
           ViewData["Cache"] = objCache;

         
            
            return View();
        }

    }
}
