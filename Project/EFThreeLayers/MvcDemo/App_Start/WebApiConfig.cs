﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MvcDemo
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //webApi路由配置
            config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/{controller}/{action}/{id}",
               defaults: new { id = RouteParameter.Optional }
           );

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}
