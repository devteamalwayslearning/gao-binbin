﻿using MvcDemo.QueueExeption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcDemo.MyError
{
    public class MyHandleErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            //错误信息
            string message =  filterContext.Exception.Message;
            //入库处理
            LogException.queue.Enqueue(filterContext.Exception);
        }
    }
}