﻿using System.Web.Mvc;

namespace MvcDemo.Areas.newArea
{
    public class newAreaAreaRegistration : AreaRegistration
    {
        //区域目标文件夹
        public override string AreaName
        {
            get
            {
                return "newArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "newArea_default",
                "newArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
