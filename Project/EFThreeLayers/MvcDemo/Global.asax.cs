﻿using MvcDemo.QueueExeption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace MvcDemo
{
    // 注意: 有关启用 IIS6 或 IIS7 经典模式的说明，
    // 请访问 http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public MvcApplication()
        {
            
        }
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //配置lonet4
            log4net.Config.XmlConfigurator.Configure();
            //开始处理记录错误
            LogException.SartWrite();
           

        }
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            HttpCookie httpCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (httpCookie == null)
            {
                return;
            }

            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                return;
            }
            //在这里数据库获取权限
            var UserRoler = "1,2,3";

            var identity = HttpContext.Current.User.Identity as FormsIdentity;
            // We could explicitly construct an Principal object with roles info using System.Security.Principal.GenericPrincipal
            var principalWithRoles = new GenericPrincipal(identity, UserRoler.Split(','));

                // Replace the user object
            HttpContext.Current.User = principalWithRoles;

        }
    }
}