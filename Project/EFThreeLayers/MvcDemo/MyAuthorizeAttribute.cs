﻿using IOC;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Providers.Entities;

namespace MvcDemo
{
    //授权过滤器
    public class MyAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            bool bAllow = false;

            //IPrincipal user = filterContext.HttpContext.User;
            //if (!user.Identity.IsAuthenticated)
            //{
            //    bAllow = false;
            //}
            //var controller = filterContext.RouteData.Values["controller"].ToString();
            //var action = filterContext.RouteData.Values["action"].ToString(); 
            //if (user.IsInRole("3"))
            //{
            //    bAllow = true;
            //}
            //if (!bAllow)
            //{
            //    filterContext.Result = new RedirectResult("~/Err.html");
            //}
            //var heads =  HttpContext.Current.Request.Headers;
            //this.IsAllowed(user, controller, action);
            //base.OnAuthorization(filterContext);

            object session = HttpContext.Current.Session["RoleID"];
            if (session == null)
            {
                filterContext.Result = new RedirectResult("~/Err.html");
            }

            string Json = HttpContext.Current.Session["RoleID"].ToString();
            if(string.IsNullOrEmpty(Json))
            {
                filterContext.Result = new RedirectResult("~/Err.html");
            }
            List<int> lstRoleID = JsonConvert.DeserializeObject<List<int>>(Json);

            string ActionName = filterContext.ActionDescriptor.ActionName;
            string ControlName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

            List<int> lstControlActionID = OperateContext.iBLLSession.iBLL_ControlActionRole.Query(s => lstRoleID.Contains(s.ID)).Select(s => s.ControlActionID).ToList();

            List<ControlAction> lstControlAction =  OperateContext.iBLLSession.iBLL_ControlAction.Query(s => lstControlActionID.Contains(s.ID)).ToList();
            //需要查找的权限
            List<ControlAction> lstFindControlAction = lstControlAction.Where(s => s.ActionName == ActionName && s.ControlName == ControlName).ToList();
            //没有权限
            if (lstFindControlAction == null || lstFindControlAction.Count== 0)
            {
                filterContext.Result = new RedirectResult("~/Err.html");
            }

        }
       private bool IsAllowed(IPrincipal user, string controllerName, string actionName) 
       {
       
           return false;
       }

    }
}