﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcDemo.Models
{
    public class Student
    {
     
        public string ID { set; get; }
        [Required]
        [Range(2,6)]
        public string Name { set; get; }
    }
}