﻿
using CodeFirstModel;
using IOC;
using IBLL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Reflection;
using BLL;
using System.Web;
using System.Web.Caching;
using System.Threading;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System.Configuration;
//[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace EFThreeLayers
{
    

    class Program
    {
        static void Main(string[] args)
        {
            UnityTest();
            //Test test = Test.instance;

            //test.a = 100;



            //Test test1 = Test.instance;
            //test1.a = 200;
            //缓存依赖
           //System.Web.Caching.Cache cache =  HttpRuntime.Cache; 
           // CacheDependency cacheDependency = new CacheDependency(@"F:\Code\GitBucketDemo\gao-binbin\Project\EFThreeLayers\EFThreeLayers\Denpence.txt");
          
           // cache.Add("1","1",cacheDependency,DateTime.Now.AddHours(1),TimeSpan.Zero,CacheItemPriority.Normal,remove);

           //     while(true)
           //     {
           //         if(cache["1"]== null)
           //         {
           //             cache.Add("1", "1", cacheDependency, DateTime.Now.AddHours(1), TimeSpan.Zero, CacheItemPriority.Normal, remove);
           //         }
           //         else
           //         {
           //             Console.Write("显示" + cache["1"]);
           //         }
           //         Thread.Sleep(100);

                
           //     }



            //Calc(DateTime.Now.AddDays(1), DateTime.Now.AddHours(1));
            //log4net测试
            //log4net.Config.XmlConfigurator.Configure();
            //ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            //log.Error("error", new Exception("发生了一个异常"));
            //log.Fatal("fatal", new Exception("发生了一个致命错误"));
            //log.Info("info");
            //log.Debug("debug");
            //log.Warn("warn");
            //Console.WriteLine("日志记录完毕。");

            //BLL和DAL层功能测试
            ////IBLL_EF_Student bLL_EF_Student = new BLL_EF_Student();
            //IBLLFactory<EF_Student> iBLLFactory = new BLLFactory<EF_Student>();

            //IBLLBase<EF_Student> bLL_EF_Student = iBLLFactory.GetFactory();
            ////BLL_EF_Teacher bLL_EF_Teacher = new BLL_EF_Teacher();

            ////bLL_EF_Student.Table();
           
            
            ////bLL_EF_Student.Hello("hellp");

            //EF_Student eF_Student = new EF_Student();
            //eF_Student.Name = "大名";
            //bLL_EF_Student.Add(eF_Student);

            //bLL_EF_Student.SqlQery();


            //bLL_EF_Student.Add(eF_Student);


            //IBLL_EF_Student obj = OperateFactory.GetObject<IBLL_EF_Student>("bll");
            //obj.QeryCheck();
       
           // IBLL_EF_Teacher obj1 = OperateFactory.GetObject<IBLL_EF_Teacher>("bll2");
           // CodelFisrtContext codelFisrtContext = new CodelFisrtContext();


          //  Student student = OperateFactory.GetObject<Student>("student");
            //codefirst测试
            //codelFisrtContext.Database.CreateIfNotExists();
            //codelFisrtContext.Code_EF_StudentContext.Create();
            //codelFisrtContext.Code_EF_TeacherContext.Create();
            //Code_EF_Student code_EF_Student1 = new Code_EF_Student();
            //code_EF_Student1.Name = "li1";
            //Code_EF_Student code_EF_Student2 = new Code_EF_Student();
            //code_EF_Student2.Name = "li1";
            //codelFisrtContext.Set<Code_EF_Student>().Add(code_EF_Student1);
            //codelFisrtContext.Set<Code_EF_Student>().Add(code_EF_Student2);
            //codelFisrtContext.SaveChanges();

        }
        /// <summary>
        /// IOC Unity 代码注入方法
        /// </summary>
        public static void UnityTest()
        {
            //1：泛型注入方式
            UnityContainer container = new UnityContainer();//创建容器
            {
                container.RegisterType<Ilog, log1>();//注册依赖对象
                Ilog ilog = container.Resolve<Ilog>();
                ilog.Hello();
            }
            //type注入方式
            {
                container.RegisterType(typeof(Ilog), typeof(log1));//注册依赖对象
                Ilog ilogtype = (Ilog)container.Resolve(typeof(Ilog));
                ilogtype.Hello();
            }
            // 同一个接口多个实现，实现方式
            {
                container.RegisterType<Ilog, log1>("1");//注册依赖对象
                container.RegisterType<Ilog, log2>("2");//注册依赖对象
                Ilog ilog1 = container.Resolve<Ilog>("1");
                ilog1.Hello();
                Ilog ilog2 = container.Resolve<Ilog>("2");
                ilog2.Hello();
            }
            //单例注册模型
            {
                //注册依赖对象为单例模型
                container.RegisterType<IlogInstance, logInstance>(new ContainerControlledLifetimeManager());
                IlogInstance ilogInstance = container.Resolve<IlogInstance>();
                ilogInstance.a = 100;

                IlogInstance ilogInstance1 = container.Resolve<IlogInstance>();
                int a = ilogInstance1.a;
                ilogInstance.a = 200;
            }

            //单例注册模型 多实现
            {
                //注册依赖对象为单例模型
                container.RegisterType<IlogInstance, logInstance>("1", new ContainerControlledLifetimeManager());
                container.RegisterType<IlogInstance, logInstance1>("2", new ContainerControlledLifetimeManager());
                IlogInstance ilogInstance = container.Resolve<IlogInstance>("1");
                ilogInstance.Hello();
                IlogInstance ilogInstance2 = container.Resolve<IlogInstance>("2");
                ilogInstance2.Hello();
            }
            //深层依赖
            {
                container.RegisterType<IlogInstance, logInner>(new ContainerControlledLifetimeManager());
                container.RegisterType<IA, A>();
                IlogInstance ilogInstance = container.Resolve<IlogInstance>();
                ilogInstance.Hello();
            }

            //深层依赖通过构造函数直接就可以注入
            {
                container.RegisterType<IlogInstance, logInner>(new ContainerControlledLifetimeManager());
                container.RegisterType<IB, B>();
                container.RegisterType<IA, A>();
                IlogInstance ilogInstance = container.Resolve<IlogInstance>();
                ilogInstance.Hello();
                IB ib = container.Resolve<IB>();
                ib.HelloB();
            }

            //深层依赖通过属性注入
            {
                container.RegisterType<IB, B2>();
                container.RegisterType<IA, A>();
                IB ib = container.Resolve<IB>();
                ib.HelloB();
            }
            //深层依赖通过方法注入
            {
                container.RegisterType<IB, B3>();
                container.RegisterType<IA, A>();
                IB ib = container.Resolve<IB>();
                ib.HelloB();
            }
            //配置文件实现反射
          
                string str4 = AppDomain.CurrentDomain.BaseDirectory;

                string configFile = str4 + @"Unity.config";
                var fileMap = new ExeConfigurationFileMap { ExeConfigFilename = configFile };
                //从config文件中读取配置信息
                Configuration configuration =
                    ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
                //获取指定名称的配置节
                UnityConfigurationSection section = (UnityConfigurationSection)configuration.GetSection("unity");

                //单例模式
                //载入名称为FirstClass 的container节点
                container.LoadConfiguration(section, "MyContainer");
                //单例模式
                //IlogInstance ilog = container.Resolve<IlogInstance>("1");
                //ilog.a = 100;
                //ilog.Hello();

                //IlogInstance ilog2 = container.Resolve<IlogInstance>("1");
                //object o = ilog2;
                //ilog.a = 100;
            {
                //非单例模式
                IlogInstance ilog = container.Resolve<IlogInstance>("2");
                ilog.a = 100;
                ilog.Hello();

                IlogInstance ilog2 = container.Resolve<IlogInstance>("2");
                object o = ilog2;
                ilog.a = 100;
            }

            {
                IC ilog = container.Resolve<IC>();

                ilog.Hello();
            }




        }


        //public static int Calc(DateTime d1, DateTime d2)
        //{
        //    DateTime max = d1 > d2 ? d1 : d2;
        //    DateTime min = d1 > d2 ? d2 : d1;

        //    int yeardiff = max.Year - min.Year;
        //    int monthdiff = max.Month - min.Month;

        //    return yeardiff * 12 + monthdiff + 1;
        //}

        private static void remove(string key, object value, CacheItemRemovedReason reason)
        { 
        
        }


        public static int Calc(DateTime d1, DateTime d2)
        {
            DateTime min ;
            DateTime max = Compare<DateTime>.compareGeneric(d1, d2,out min);
            int yeardiff = max.Year - min.Year;
            int monthdiff = max.Month - min.Month;

            return yeardiff * 12 + monthdiff + 1;
        }
        //泛型比大小
        public class Compare<T> where T : IComparable
        {
            //使用泛型实现的比较方法
            public static T compareGeneric(T t1, T t2,out T min)
            {
                if (t1.CompareTo(t2) > 0)
                {
                    min = t2;
                    return t1;
                }
                else
                {
                    min = t1;
                    return t2;
                }
            }
        }

    }
}
