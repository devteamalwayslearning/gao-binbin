﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFThreeLayers
{
    public  class Test
    {
        public static Test _test;
        public static Test instance
        {
            get
            {
                if (_test == null)
                {
                    _test = new Test();
                }
                return _test;
            }
        }

        public int a { set; get; }
    }
}
