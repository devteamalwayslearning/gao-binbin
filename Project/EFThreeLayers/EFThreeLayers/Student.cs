﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFThreeLayers
{
    public class Student
    {
        public string P1 { set; get; }

        public List<Teacher> lstTeacher { set; get; }
        public List<string> lst { set; get; }
        public Dictionary<string, string> dic { set; get; }

        public Teacher teacher { set; get; }

        public Dictionary<Teacher, Teacher> dicTeacher { set; get; }
    }

    public class Teacher
    {
        public string Name { set; get; }
    }
}
