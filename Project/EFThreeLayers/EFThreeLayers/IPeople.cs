﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFThreeLayers
{
     /// <summary>
    /// 人接口
    /// </summary>
    public interface IPeople
    {
        void DrinkWater();
    }
    /// <summary>
    /// 村民
    /// </summary>
    public class VillagePeople : IPeople
    {
        IWaterTool _pw;
        public VillagePeople(IWaterTool pw)
        {
            _pw = pw;
        }
        public void DrinkWater()
        {
            Console.WriteLine(_pw.returnWater());
        }
    }
    /// <summary>
    /// 压水井
    /// </summary>
    public class PressWater : IWaterTool
    {
        public string returnWater()
        {
            return "地下水好甜啊！！！";
        }
    }
    /// <summary>
    /// 获取水方式接口
    /// </summary>
    public interface IWaterTool
    {
        string returnWater();
    }
}
