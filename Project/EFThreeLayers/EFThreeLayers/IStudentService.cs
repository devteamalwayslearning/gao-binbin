﻿using Spring.Aop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFThreeLayers
{
    public interface IStudentService
    {
        void GoToSchool(string studentName, string className);
    }

    public class StudentService : IStudentService
    {
        public void GoToSchool(string studentName, string className)
        {
            Console.WriteLine("计算机({1})班的{0}同学去上学了。。。", studentName, className);
        }
    }

    public class LogBeforeAdvice : IMethodBeforeAdvice
    {
        public void Before(System.Reflection.MethodInfo method, object[] args, object target)
        {
            Console.WriteLine("拦截的方法名—>" + method.Name);
            Console.WriteLine("目标—>" + target);
            Console.WriteLine("参数—>");
            if (args != null)
            {
                foreach (object arg in args)
                {
                    Console.WriteLine("\t: " + arg);
                }
            }
        }
    }  
}
