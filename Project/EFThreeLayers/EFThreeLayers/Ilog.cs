﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFThreeLayers
{
    public interface Ilog
    {

        void Hello();
    }

    public class log1 : Ilog
    {
        public void Hello()
        {
            Console.Write("1");
        }
    }

    public class log2 : Ilog
    {
        public void Hello()
        {
            Console.Write("2");
        }
    }

    public interface IlogInstance
    {
        int a { set; get; }
        void Hello();
    }

    public class logInstance : IlogInstance
    {
        public int _a;
        public int a
        {
            set
            {
                _a = value;
            }
            get
            {
                return _a;
            }
        }

        public void Hello()
        {
            Console.Write("1");
        }
    }

    public class logInstance1 : IlogInstance
    {
        public int _a;
        public int a
        {
            set
            {
                _a = value;
            }
            get
            {
                return _a;
            }
        }

        public void Hello()
        {
            Console.Write("2");
        }
    }





    public class logInner:IlogInstance
    {

        public IA _ia;

        public logInner(IA ia)
        {
            _ia = ia;
        }

        public int _a;
        public int a
        {
            set
            {
                _a = value;
            }
            get
            {
                return _a;
            }
        }

        public void Hello()
        {
            _ia.AHello();
            Console.Write("3");
        }
    }


    public interface IA
    {
        void AHello();
    }

    public class A : IA
    {
        public void AHello()
        {
            Console.Write("A");
        }
    }


    public interface IB
    {
        void HelloB();
    }


    public class B2 : IB
    {
       [Dependency]
        public IA _ia{set;get;}
      

        public void HelloB()
        {
            Console.Write("B");
            _ia.AHello();
        }
    }
    public class B : IB
    {
        private IA _ia;
        private string _a;
        public B(IA ia)
        {
            _ia = ia;
        }
    
        public void HelloB()
        {
            Console.Write("B");
            _ia.AHello();
        }
    }

    public class B3 : IB
    {
        private IA _ia;
        //自定义注入函数
        [InjectionMethod]
        public void Inital(IA ia)
        {
            _ia = ia;
        }
        //可以有无参构造函数应对
        public B3()
        { 
        }
        //[InjectionConstructor]
        //public B3(IA ia)
        //{
        //    _ia = ia;
        //}
    
        public void HelloB()
        {
            _ia.AHello();
            Console.Write("B3");
        }
    }

    public interface IC
    {
        void Hello();
    }

    public class C : IC
    {
        //添加依赖和代码注入一样
        //[Dependency]
        private string _a;
        public IA _ia;
        [InjectionConstructor]
        public C(IA ia)
        {
            _ia = ia;
        }

        public C(string a)
        {
            _a = a;
        }
        public void Hello()
        {
            Console.Write("1");
            _ia.AHello();
        }
    }





}
