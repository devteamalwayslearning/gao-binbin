﻿using DAL;
using IBLL;
using Spring.Context;
using Spring.Context.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC
{
    public class OperateFactory
    {
        private static IApplicationContext SpringContext
        {
            get
            {
                return ContextRegistry.GetContext();
            }
        }

        public static T GetObject<T>(string objName)where T:class
        {
            return (T)SpringContext.GetObject(objName);
        }
    }
}
