﻿using Aspose.Words;
using Novacode;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldToPdfA
{
    public class DocHelper
    {
        //两个图片章
        private static readonly string operatorSealImage = @"E:\app.jpg";
        private static readonly string financeSealImage = @"E:\ggg.jpg";
        //模板文档
        private static readonly string temperDoc = @"E:\Temper.doc";

        //选中字符
        private static string Checked = "☑";
        //未选中字符
        private static string UnChecked = "□";
        //测试出宽度
        private static int minRightWidth = 15;
        //高度根据表格高度获得
        private static int Hight = 80;
        //锁
        private static object Lock = new object();


        public static void CreatePdf(List<InsertData> lstInsertData, List<bool> lstCheck, string woldFileName, string copyTemperFile)
        {

            if (lstInsertData == null || lstInsertData.Count == 0 || lstInsertData.Count > 2
                || lstCheck == null || lstCheck.Count != 3)
            {
                throw new ArgumentException("lstInsertData or lstCheck Err");
            }

            DocX doc = DocX.Load(copyTemperFile);
            //DocX doc = DocX.Load(@"E:\aaa&.doc");
            int tableCount = doc.Tables.Count;
            if (tableCount == 0)
            {
                throw new ArgumentException("Table lost");
            }
            //模板中只有一个Table
            Table table = doc.Tables[0];
            if (table == null)
            {
                throw new ArgumentException("Table is null");
            }

            int nCountlstInsertData = lstInsertData.Count;
            //开始填写行数
            int nStartWriteRow = 2;

            for (int i = nStartWriteRow, j = 0; i < nCountlstInsertData + nStartWriteRow; i++, j++)
            {
                //全卡号
                var CardNum = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].CardNum))
                {
                    CardNum = lstInsertData[j].CardNum;
                }
                table.Rows[i].Cells[0].Paragraphs[0].InsertText(CardNum);
                //建卡时间
                var CardTime = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].CardTime))
                {
                    CardTime = lstInsertData[j].CardTime;
                }
                table.Rows[i].Cells[1].Paragraphs[0].InsertText(CardTime);
                //刷卡金额
                var CardAmount = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].CardAmount))
                {
                    CardAmount = lstInsertData[j].CardAmount;
                }
                table.Rows[i].Cells[2].Paragraphs[0].InsertText(CardAmount);
                //持退卡金额小写
                var LowerCardAmount = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].LowerCardAmount))
                {
                    LowerCardAmount = lstInsertData[j].LowerCardAmount;
                }
                table.Rows[i].Cells[3].Paragraphs[0].InsertText(LowerCardAmount);
                //持退卡金额大写
                var UpCardAmount = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].UpCardAmount))
                {
                    UpCardAmount = lstInsertData[j].UpCardAmount;
                }
                table.Rows[i].Cells[4].Paragraphs[0].InsertText(UpCardAmount);
                //交易参考号
                var TransReferNumber = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].TransReferNumber))
                {
                    TransReferNumber = lstInsertData[j].TransReferNumber;
                }
                table.Rows[i].Cells[5].Paragraphs[0].InsertText(TransReferNumber);
                //调帐原因
                var TransfeReason = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].TransfeReason))
                {
                    TransfeReason = lstInsertData[j].TransfeReason;
                }
                table.Rows[i].Cells[6].Paragraphs[0].InsertText(TransfeReason);
            }

            //获取所有的段落
            List<Novacode.Paragraph> lstParagraph = table.Rows[6].Cells[1].Paragraphs.ToList();

            bool bAudit = lstCheck[0];
            if (bAudit)
            {
                //获取审核无误
                Novacode.Paragraph auditParagraph = lstParagraph.Find(s => s.Text.Contains("□审核无误"));
                string strAuditTxt = auditParagraph.Text;
                strAuditTxt = strAuditTxt.Replace(UnChecked, Checked);
                auditParagraph.ReplaceText(UnChecked, Checked);
            }
            //瑕疵
            bool bFlaw = lstCheck[1];
            if (bFlaw)
            {
                //瑕疵单据
                Novacode.Paragraph flawParagraph = lstParagraph.Find(s => s.Text.Contains("□瑕疵单据"));
                string strFlawTxt = flawParagraph.Text;
                strFlawTxt = strFlawTxt.Replace(UnChecked, Checked);
                flawParagraph.ReplaceText(UnChecked, Checked);
            }

            //其他
            bool bOther = lstCheck[2];
            if (bOther)
            {
                Novacode.Paragraph otherParagraph = lstParagraph.Find(s => s.Text.Contains("□其它"));
                string strOtherTxt = otherParagraph.Text;
                strOtherTxt = strOtherTxt.Replace(UnChecked, Checked);
                otherParagraph.ReplaceText(UnChecked, Checked);
            }

            //经办人签章
            Novacode.Image operatorSealImg = doc.AddImage(operatorSealImage); // Create image.
            Picture operatorPicture = operatorSealImg.CreatePicture();     // Create picture.
            operatorPicture.Height = Hight;
            operatorPicture.Width = (int)table.Rows[7].Cells[0].Width - minRightWidth;
            Novacode.Paragraph p = table.Rows[2].Cells[1].Paragraphs[0];
            List<Novacode.Paragraph> newlstParagraph = table.Rows[9].Cells[0].Paragraphs.ToList();
            table.Rows[9].Cells[0].Paragraphs[0].RemoveText(0, false);
            table.Rows[9].Cells[0].Paragraphs[0].InsertPicture(operatorPicture).Alignment = Alignment.left;
            table.Rows[9].Cells[0].VerticalAlignment = VerticalAlignment.Center;

            //财务盖章

            Novacode.Image financeSealImg = doc.AddImage(financeSealImage); // Create image.
            Picture financePicture = financeSealImg.CreatePicture();     // Create picture.
            financePicture.Height = Hight;
            financePicture.Width = (int)table.Rows[7].Cells[1].Width - minRightWidth;
            table.Rows[9].Cells[1].Paragraphs[0].RemoveText(0, false);
            table.Rows[9].Cells[1].Paragraphs[0].InsertPicture(financePicture).Alignment = Alignment.left;
            table.Rows[9].Cells[1].VerticalAlignment = VerticalAlignment.Center;

            doc.SaveAs(woldFileName);
            doc.Dispose();

            if (!File.Exists(woldFileName))
            {
                return;
            }

            string wordExtension = Path.GetExtension(woldFileName);

            string pdfFileName = woldFileName.Replace(wordExtension,".pdf");

            Document ConvertPdf = new Document(woldFileName);
            ConvertPdf.Save(pdfFileName, SaveFormat.Pdf);

            if (!File.Exists(woldFileName))
            {
                return;
            }

            File.Delete(woldFileName);
        }
        //先复制出来然后再删除模板
        public static void CopyTemplate(List<InsertData> lstInsertData, List<bool> lstCheck, string FileName)
        {
            if (string.IsNullOrEmpty(FileName))
            {
                return;
            }
          
            Guid guid = Guid.NewGuid();
            string copyTemperFile = Path.GetFileName(temperDoc);
            //为每个线程生成模版
            copyTemperFile = guid.ToString() + copyTemperFile;

            copyTemperFile = Path.GetDirectoryName(temperDoc) + copyTemperFile;

            File.Copy(temperDoc, copyTemperFile);
            CreatePdf(lstInsertData, lstCheck, FileName, copyTemperFile);
            File.Delete(copyTemperFile);
        }

    }
}
