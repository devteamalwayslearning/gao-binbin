﻿using Aspose.Words;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldToPdfA.WorldToPdf
{
    public class WordToPdf : IWordToPdf
    {
        private static readonly WordToPdf instance = null;  
        static WordToPdf()  
       {  
           instance = new WordToPdf();  
       }
        private WordToPdf()  
       {  
       }
        public static WordToPdf Instance  
       {  
           get  
           {  
               return instance;  
           }  
       }  
        //模板表格
        protected readonly string Template = @"E:\Temper - 1.doc";
        //模板标题占位符

        protected readonly string SymbolMerchantName = "AAAAAA";

        protected readonly string SymbolMerchantNumber = "BBBBBB";

        protected readonly string SymbolTerminalNumber = "CCCCCC";

        protected readonly string SymbolYearhLast = "D";


        protected readonly string SymbolMonth = "EF";

        protected readonly string SymbolDate = "GH";

        ////两个图片章
        //protected  readonly string operatorSealImage = @"E:\app.jpg";
        //protected  readonly string financeSealImage = @"E:\ggg.jpg";
        //表格左侧边界
        protected readonly int rightMangin = 10;
        //图片高度
        protected readonly int pictureHight = 30;
        public void TransformWord(List<InsertData> lstInsertData, TitleData titleData, string fileName, string operateName)
        {
            if (lstInsertData == null || lstInsertData.Count == 0 || titleData == null
               || string.IsNullOrEmpty(fileName))
           {
               throw new ArgumentNullException("lstInsertData or lstCheck is null or Count is 0");
           }
            CopyTemplate(lstInsertData, titleData, fileName, operateName);
        }


        protected virtual void CopyTemplate(List<InsertData> lstInsertData, TitleData titleData, string fileName, string operateName)
        {

            Guid guid = Guid.NewGuid();
            string copyTemplateFile = Path.GetFileName(Template);
            //为每个线程生成模版
            copyTemplateFile = guid.ToString() + copyTemplateFile;
            copyTemplateFile = Path.GetDirectoryName(Template) + copyTemplateFile;
            File.Copy(Template, copyTemplateFile);
            this.CreatTable(lstInsertData, titleData, fileName, copyTemplateFile, operateName);
            if (!File.Exists(copyTemplateFile))
            {
                return;
            }
            //删除复制模板
            File.Delete(copyTemplateFile);
        }



        protected virtual void CreatTable(List<InsertData> lstInsertData, TitleData titleData, string fileName, string copyTemplateFile, string operateName)
        {
            if (!File.Exists(copyTemplateFile))
            {
                throw new ArgumentNullException("copyTemplateFile is lost");
            }

            Aspose.Words.Document doc = new Aspose.Words.Document(copyTemplateFile);
            DocumentBuilder builder = new DocumentBuilder(doc);
            //添加标题头
            NodeCollection paragraphs =  doc.GetChildNodes(NodeType.Paragraph, true);

            Aspose.Words.Paragraph pTitle = (Aspose.Words.Paragraph)paragraphs[1];

            RunCollection pRun = pTitle.Runs;

            foreach (Aspose.Words.Run Item in pRun)
            {
                //商户名称
                if (!string.IsNullOrEmpty(titleData.MerchantName))
                {
                    if (Item.Text.Contains(SymbolMerchantName))
                    {
                        Item.Text = titleData.MerchantName;
                    }
                }
                //商户编号
                if (!string.IsNullOrEmpty(titleData.MerchantNumber))
                {
                    if (Item.Text.Contains(SymbolMerchantNumber))
                    {
                        Item.Text = titleData.MerchantNumber;
                    }
                }
                //终端号
                if (!string.IsNullOrEmpty(titleData.TerminalNumber))
                {
                    if (Item.Text.Contains(SymbolTerminalNumber))
                    {
                        Item.Text = titleData.TerminalNumber;
                    }
                }
                //年
                if (!string.IsNullOrEmpty(titleData.YearhLast))
                {
                    if (Item.Text.Contains(SymbolYearhLast))
                    {
                        Item.Text = titleData.YearhLast;
                    }
                }
                //月
                if (!string.IsNullOrEmpty(titleData.Month))
                {
                    if (Item.Text.Contains(SymbolMonth))
                    {
                        Item.Text = titleData.Month;
                    }
                }
                //日
                if (!string.IsNullOrEmpty(titleData.Date))
                {
                    if (Item.Text.Contains(SymbolMonth))
                    {
                        Item.Text = titleData.Month;
                    }
                }
            
            }



            NodeCollection tables = doc.GetChildNodes(NodeType.Table, true);
            Aspose.Words.Tables.Table table = (Aspose.Words.Tables.Table)tables[0];
            int nCount = lstInsertData.Count;
            for (int i = 2, j = 0; i < nCount + 2; i++,j++)
            {
                //全卡号
                var CardNum = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].CardNum))
                {
                    CardNum = lstInsertData[j].CardNum;
                }
                builder.MoveToCell(0, i, 0, 0);
                builder.Write(CardNum);
                //建卡时间
                var CardTime = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].CardTime))
                {
                    CardTime = lstInsertData[j].CardTime;
                }
                builder.MoveToCell(0, i, 1, 0);
                builder.Write(CardTime);              
                //刷卡金额
                var CardAmount = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].CardAmount))
                {
                    CardAmount = lstInsertData[j].CardAmount;
                }
                builder.MoveToCell(0, i, 2, 0);
                builder.Write(CardAmount); 
                //持退卡金额小写
                var LowerCardAmount = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].LowerCardAmount))
                {
                    LowerCardAmount = lstInsertData[j].LowerCardAmount;
                }
                builder.MoveToCell(0, i, 3, 0);
                builder.Write(LowerCardAmount); 
                //持退卡金额大写
                var UpCardAmount = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].UpCardAmount))
                {
                    UpCardAmount = lstInsertData[j].UpCardAmount;
                }
                builder.MoveToCell(0, i, 4, 0);
                builder.Write(UpCardAmount); 
                //交易参考号
                var TransReferNumber = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].TransReferNumber))
                {
                    TransReferNumber = lstInsertData[j].TransReferNumber;
                }
                builder.MoveToCell(0, i, 5, 0);
                builder.Write(TransReferNumber);
                //调帐原因
                var TransfeReason = string.Empty;
                if (!string.IsNullOrEmpty(lstInsertData[j].TransfeReason))
                {
                    TransfeReason = lstInsertData[j].TransfeReason;
                }
                builder.MoveToCell(0, i, 6, 0);
                builder.Write(TransfeReason);
            }


            Aspose.Words.Paragraph[] para = table.Rows[6].Cells[0].Paragraphs.ToArray();

            ////现在(北京)支付有限公司填写
            //Aspose.Words.Paragraph[] para = table.Rows[6].Cells[1].Paragraphs.ToArray();
            ////审核无误

            //bool bAudit = lstCheck[0];
            //if (bAudit)
            //{
            //    string strAudit = para[2].Runs[0].Text;
            //    strAudit = strAudit.Replace("□", "☑");
            //    para[2].Runs[0].Text = strAudit;
            //}

            ////瑕疵
            //bool bFlaw = lstCheck[1];
            //if (bFlaw)
            //{
            //    string strFlaw = para[3].Runs[0].Text;
            //    strFlaw = strFlaw.Replace("□", "☑");
            //    para[3].Runs[0].Text = strFlaw;
            //}

            ////其他
            //bool bOther = lstCheck[2];
            //if (bOther)
            //{
            //    string strOther = para[4].Runs[0].Text;
            //    strOther = strOther.Replace("□", "☑");
            //    para[4].Runs[0].Text = strOther;
            //}
            //string Name = "张三";

            //经办人签章
            Aspose.Words.Paragraph[] ParaOperatorSeal = table.Rows[7].Cells[0].Paragraphs.ToArray();
            string OperatorSeal =  ParaOperatorSeal[0].Runs[0].Text;
            ParaOperatorSeal[0].Runs[0].Text = OperatorSeal +":"+ operateName;


            //底下日期
            Aspose.Words.Paragraph[] ParaDate = table.Rows[8].Cells[0].Paragraphs.ToArray();
            string Date = ParaDate[0].Runs[0].Text;
            ParaDate[0].Runs[0].Text = Date + titleData.LowerDate;

            ////财务盖章
            //Aspose.Words.Paragraph[] ParaFinanceSeal = table.Rows[7].Cells[1].Paragraphs.ToArray();
            //ParaFinanceSeal[0].Runs[0].Text = "";
            //builder.MoveToCell(0, 7, 1, 0);
            //builder.InsertImage(financeSealImage, builder.CellFormat.Width - rightMangin, pictureHight);
            doc.Save(fileName, SaveFormat.Pdf);
        }
    }
}
