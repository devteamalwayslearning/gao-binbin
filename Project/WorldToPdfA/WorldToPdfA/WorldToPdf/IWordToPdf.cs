﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldToPdfA
{
    public interface IWordToPdf
    {
        void TransformWord(List<InsertData> lstInsertData, TitleData titleData, string fileName, string operateName);
    }
}
