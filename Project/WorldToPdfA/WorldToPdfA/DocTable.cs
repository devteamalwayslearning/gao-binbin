﻿using Novacode;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldToPdfA
{
    /// <summary>
    /// 内存中生成word
    /// </summary>
    public class DocTable
    {
        public static void CreateTable()
        {
            DocX doc = DocX.Create(@"E:\Temper.doc");     
            //首先创建1个格式对象 
            Formatting formatting = new Formatting(); 
            formatting.Bold = true; 
            formatting.Size = 16;
            formatting.FontColor = Color.Black;
            //将文本插入到指定位置，并控制格式 
            Paragraph p = doc.InsertParagraph("现在(北京)支付股份有限公司特约商户调账申请表", false, formatting);
            p.Alignment = Alignment.center;
            p.StyleName = "Heading1";
            doc.InsertParagraph("用户名称：            编号：            终端号：        201 年 月 日");
            Paragraph p2 = doc.Paragraphs[1];
            p2.Alignment = Alignment.left;
            Table table = doc.AddTable(3,7);
            table.Design = TableDesign.TableGrid;
            table.Alignment = Alignment.center;
            //先处理完表格再插入数据
            table.MergeCellsInColumn(0, 0, 1);
            table.MergeCellsInColumn(1, 0, 1);
            table.MergeCellsInColumn(2, 0, 1);

            table.MergeCellsInColumn(5, 0, 1);
            table.MergeCellsInColumn(6, 0, 1);
            //标题
            table.Rows[0].Cells[0].Paragraphs[0].InsertText("卡号全卡号");
            table.Rows[0].Cells[0].Paragraphs[0].Alignment = Alignment.center;
            table.Rows[0].Cells[1].Paragraphs[0].InsertText("交易日期刷卡时间");
            table.Rows[0].Cells[1].Paragraphs[0].Alignment = Alignment.center;
            table.Rows[0].Cells[2].Paragraphs[0].InsertText("原交易金额");
            table.Rows[0].Cells[2].Paragraphs[0].Alignment = Alignment.center;
            table.Rows[0].Cells[3].Paragraphs[0].InsertText("调帐金额");
            table.Rows[0].Cells[3].Paragraphs[0].Alignment = Alignment.center;
            table.Rows[0].Cells[5].Paragraphs[0].InsertText("交易参考号");
            table.Rows[0].Cells[5].VerticalAlignment = VerticalAlignment.Center;
            table.Rows[0].Cells[6].Paragraphs[0].InsertText("调帐原因");
            table.Rows[0].Cells[6].VerticalAlignment = VerticalAlignment.Center;
            table.Rows[0].MergeCells(3,4);
            table.Rows[0].Cells[3].RemoveParagraphAt(1);
            //填写小空格
            table.Rows[1].Cells[3].Paragraphs[0].InsertText("小写");
            table.Rows[1].Cells[3].Paragraphs[0].Alignment = Alignment.center;
            table.Rows[1].Cells[4].Paragraphs[0].InsertText("大写");
            table.Rows[1].Cells[4].Paragraphs[0].Alignment = Alignment.center;
            //标题
            table.Rows[2].Cells[0].Paragraphs[0].InsertText("全卡号");
            table.Rows[2].Cells[0].Paragraphs[0].Alignment = Alignment.center;
            table.Rows[2].Cells[1].Paragraphs[0].InsertText("建卡时间");
            table.Rows[2].Cells[1].Paragraphs[0].Alignment = Alignment.center;

            table.Rows[2].Cells[2].Paragraphs[0].InsertText("刷卡金额");
            table.Rows[2].Cells[2].Paragraphs[0].Alignment = Alignment.center;
            table.Rows[2].Cells[3].Paragraphs[0].InsertText("退持卡金额");
            table.Rows[2].Cells[3].Paragraphs[0].Alignment = Alignment.center;

            table.Rows[2].Cells[4].Paragraphs[0].InsertText("与小写对应");
            table.Rows[2].Cells[4].Paragraphs[0].Alignment = Alignment.center;

            table.Rows[2].Cells[5].Paragraphs[0].InsertText("12位");
            table.Rows[2].Cells[5].Paragraphs[0].Alignment = Alignment.center;

            table.Rows[2].Cells[6].Paragraphs[0].InsertText("见红字");
            table.Rows[2].Cells[6].Paragraphs[0].Alignment = Alignment.center;

            //数据3个表格
            //3
            table.InsertRow(); 
            //4
            table.InsertRow();
            //5
            table.InsertRow();
            //我单位保证上述属实,6
            table.InsertRow();
            table.Rows[6].MergeCells(0, 6);
            table.Rows[6].Height = table.Rows[5].Height;

            List<Paragraph> lstParagraph1 = table.Rows[6].Paragraphs.ToList();
            for (int i = 1; i < lstParagraph1.Count; i++)
            {
                table.Rows[6].RemoveParagraph(lstParagraph1[i]);
            }

            table.Rows[6].Cells[0].Paragraphs[0].InsertText("我单位保证上述属实");
            table.Rows[6].Cells[0].Paragraphs[0].Alignment = Alignment.center;
            //

            //我单位保证上述属实,6
            table.InsertRow();
            table.Rows[7].MergeCells(0, 6);
            table.Rows[7].Height = table.Rows[5].Height;

            List<Paragraph> lstParagraph2 = table.Rows[7].Paragraphs.ToList();
            for (int i = 1; i < lstParagraph2.Count; i++)
            {
                table.Rows[7].RemoveParagraph(lstParagraph2[i]);
            }

            table.Rows[7].Cells[0].Paragraphs[0].InsertText("注：调帐原因’’’’’");
            table.Rows[7].Cells[0].Paragraphs[0].Alignment = Alignment.center;
            //合并表格顺序很重要
            //图片表
            table.InsertRow();//8
            table.InsertRow();//9
            //table.Rows[8].MergeCells(0, 3);
            //table.Rows[9].MergeCells(0, 3);

            table.Rows[8].MergeCells(4,6);
            table.Rows[9].MergeCells(4, 6);
            //先合上下
            table.MergeCellsInColumn(4,8,9);
            table.Rows[8].MergeCells(0, 3);
            table.Rows[9].MergeCells(0, 1);
            table.Rows[9].MergeCells(1, 2);
            doc.InsertTable(table);
            doc.Save();
        }
    }
}
