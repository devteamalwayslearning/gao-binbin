﻿using Microsoft.Office.Interop.Word;
using Microsoft.Vbe.Interop;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WorldToPdfA
{
    public class WordHelper
    {
        //经办人盖章
        private static readonly string operatorSealImagePath = @"E:\app.jpg";
        //财务盖章
        private static readonly string financeSealImagePath = @"E:\app.jpg";
        //模板文档
        private static readonly string templateWorldPath = @"E:\Temper.doc";
        //源模板地址
        private static readonly string sourceTemplateWorldPath = @"E:\aaa&Copy.doc";
        //生成PDF文件
        private static readonly string outPdfPath = @"E:\PDF.pdf";
        //文件夹里内存生成文件清除
        private static readonly string filePath = @"E:";

        //world操作代码
        public static void CreateWordFile(List<InsertData> lstInsertData,List<bool> lstChecked)
        {
            //选中字符
            string Checked = "☑";
            //未选中字符
            string UnChecked = "□";

            //数据最多有3条
            if (lstInsertData == null || lstChecked == null || lstInsertData.Count == 0 ||
                lstInsertData.Count > 2 || lstChecked.Count ==0)
            {
                return;    
            }
            Microsoft.Office.Interop.Word.Application wordApp = null;
            // 打开Word文档
            Microsoft.Office.Interop.Word.Document wordDoc = null; 

            try
            {
                //右边沿暂时这么解决具体测试
                var rightWidthedge = 0;
                var oMissing = System.Reflection.Missing.Value;
                //创建Word应用程序
                wordApp = new ApplicationClass();
                // 打开Word文档
                wordDoc = wordApp.Documents.Open(templateWorldPath); 
                //模板表格格式锁定
                Microsoft.Office.Interop.Word.Table table = wordDoc.Tables[1];
                int Count = lstInsertData.Count + 4;
                //表格填值
                for (int i = 4, j = 0; i < Count; i++, j++)
                {
                    //全卡号
                    var CardNum = string.Empty;
                    if (!string.IsNullOrEmpty(lstInsertData[j].CardNum))
                    {
                        CardNum = lstInsertData[j].CardNum;
                    }

                    table.Cell(i, 1).Range.Text = CardNum;
                    //刷卡时间
                    var CardTime = string.Empty;
                    if (!string.IsNullOrEmpty(lstInsertData[j].CardTime))
                    {
                        CardTime = lstInsertData[j].CardTime;
                    }
                    table.Cell(i, 2).Range.Text = CardTime;

                    //刷卡金额
                    var CardAmount = string.Empty;
                    if (!string.IsNullOrEmpty(lstInsertData[j].CardAmount))
                    {
                        CardAmount = lstInsertData[j].CardAmount;
                    }
                    table.Cell(i, 3).Range.Text = CardAmount;

                    //持退卡金额小写
                    var LowerCardAmount = string.Empty;
                    if (!string.IsNullOrEmpty(lstInsertData[j].LowerCardAmount))
                    {
                        LowerCardAmount = lstInsertData[j].LowerCardAmount;
                    }
                    table.Cell(i, 4).Range.Text = LowerCardAmount;

                    //持退卡金额大写
                    var UpCardAmount = string.Empty;
                    if (!string.IsNullOrEmpty(lstInsertData[j].UpCardAmount))
                    {
                        UpCardAmount = lstInsertData[j].UpCardAmount;
                    }
                    table.Cell(i, 5).Range.Text = UpCardAmount;

                    //交易参考号
                    var TransReferNumber = string.Empty;
                    if (!string.IsNullOrEmpty(lstInsertData[j].TransReferNumber))
                    {
                        TransReferNumber = lstInsertData[j].TransReferNumber;
                    }
                    table.Cell(i, 6).Range.Text = TransReferNumber;

                    //调帐原因
                    var TransfeReason = string.Empty;
                    if (!string.IsNullOrEmpty(lstInsertData[j].TransfeReason))
                    {
                        TransfeReason = lstInsertData[j].TransfeReason;
                    }
                    table.Cell(i, 7).Range.Text = TransfeReason;
                }

                //客户联系电话
                string customerContact = table.Cell(9, 1).Range.Text;
                //审批意见
                var CheckApproval = table.Cell(9, 2).Range.Text;
                //已经检查过的意见
                var CheckApprovaled = string.Empty;
                CheckApproval = CheckApproval.Replace(UnChecked, "&");

                string[] arrayCheckApproval = CheckApproval.Split('&');

                List<string> lstCheck = new List<string>();
                foreach (var item in lstChecked)
                {
                    if (item == true)
                    {
                        lstCheck.Add(Checked);
                    }
                    else
                    {
                        lstCheck.Add(UnChecked);
                    }
                }
                int CheckCount = lstChecked.Count;
                for (int i = 0; i < arrayCheckApproval.Length; i++)
                {
                    CheckApprovaled += arrayCheckApproval[i];
                    if (i < CheckCount)
                    {
                        CheckApprovaled += lstChecked[i];
                    }
                }

                table.Cell(9, 2).Range.Text = CheckApprovaled;
                //string operatorSealText = table.Cell(10, 2).Range.Text;
                //此模块格式固定
                //经办人签章
                string operatorSeal = table.Cell(11, 1).Range.Text;
                table.Cell(11, 1).Select();
                float operatorSealWidth = table.Cell(11, 1).Width - rightWidthedge;
                float operatorSealHeight = table.Cell(11, 1).Height;
                object linkToFile = false;
                object rangeSelectOperator = wordApp.Selection.Range;
                object saveWithDocument = true;
                Microsoft.Office.Interop.Word.InlineShape shapeOperatorSeal = 
                    wordApp.ActiveDocument.InlineShapes.AddPicture(operatorSealImagePath, ref linkToFile, ref saveWithDocument, ref rangeSelectOperator);
                shapeOperatorSeal.Width = operatorSealWidth;//图片宽度
                shapeOperatorSeal.Height = operatorSealHeight;//图片高度
                shapeOperatorSeal.ConvertToShape().WrapFormat.Type = Microsoft.Office.Interop.Word.WdWrapType.wdWrapBehind;
                //将其至于空格中
                shapeOperatorSeal.ConvertToShape().RelativeHorizontalPosition = 
                    WdRelativeHorizontalPosition.wdRelativeHorizontalPositionInnerMarginArea;
                string financeSeal = table.Cell(11, 2).Range.Text;
                table.Cell(11, 2).Select();
                object rangeSelectFinance = wordApp.Selection.Range;
                float financeSealHight = table.Cell(11, 2).Height;
                float financeSealWidth = table.Cell(11, 2).Width - rightWidthedge;

                Microsoft.Office.Interop.Word.InlineShape shapeFinanceSeal = 
                    wordApp.ActiveDocument.InlineShapes.AddPicture(financeSealImagePath, ref linkToFile, ref saveWithDocument, ref rangeSelectFinance);
                shapeFinanceSeal.Width = financeSealWidth;//图片宽度
                shapeFinanceSeal.Height = financeSealHight;//图片高度
                shapeFinanceSeal.ConvertToShape().WrapFormat.Type = Microsoft.Office.Interop.Word.WdWrapType.wdWrapBehind;
                //将其至于空格中
                shapeFinanceSeal.ConvertToShape().RelativeHorizontalPosition =
                    WdRelativeHorizontalPosition.wdRelativeHorizontalPositionInnerMarginArea;
                //设置为不可见
                wordApp.Visible = false;
                //保存world
                wordDoc.SaveAs2(@"E:\bbb.doc");//保存
                //导出Pdf
                wordDoc.ExportAsFixedFormat(outPdfPath, WdExportFormat.wdExportFormatPDF);
                //关闭
                wordDoc.Close(true);
                //释放Word进程
                wordApp.Quit();
            }
            catch(Exception ex)
            {
                //出错关闭
                if (wordDoc != null)
                {
                    wordDoc.Close();
                }

                if (wordApp != null)
                {
                    wordApp.Quit();
                }
            }
        }
        //杀死进程代码
        public static void killWinWordProcess()
        {
            //首先将残留进程杀死
            System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName("WINWORD");
            foreach (System.Diagnostics.Process process in processes)
            {
                process.Kill();
            }
            //将文件模板拷贝一份
            DirectoryInfo directoryInfo = new DirectoryInfo(filePath);
            FileInfo[] fileInfo = directoryInfo.GetFiles();
            List<FileInfo> lstFileInfo = new List<FileInfo>(fileInfo);
            var templateWorldName = Path.GetFileName(templateWorldPath);
            var TempFile = lstFileInfo.Find(s => Path.GetFileName(s.Name) == templateWorldName);
            if (TempFile == null)
            {
                return;
            }
            //一旦进程被杀死，要将文件删除，防止文件损坏
            File.Delete(TempFile.FullName);
            //重新复制出模板
            File.Copy(sourceTemplateWorldPath, templateWorldPath);

        }  
    }
    //插入卡的数据格式,数据必须和表格一致
    public class InsertData
    {
        //全卡号
        public string CardNum { set; get; }
        //建卡时间
        public string CardTime { set; get; }
        //刷卡金额
        public string CardAmount { set; get; }
        //持退卡金额小写
        public string LowerCardAmount { set; get; }
        //持退卡金额大写
        public string UpCardAmount { set; get; }
        //交易参考号
        public string TransReferNumber { set; get; }
        //调帐原因
        public string TransfeReason { set; get; }
    }

    public class TitleData
    {
        //商户名称
        public string MerchantName { set; get; }
        //商户编号
        public string MerchantNumber { set; get; }
        //终端号
        public string TerminalNumber { set; get; }
        //年最后一位
        public string YearhLast { set; get; }
        //月
        public string Month { get; set; }
        //日
        public string Date { set; get; }
        //底下日期
        public string LowerDate { set; get; }

    }

}
